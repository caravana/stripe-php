<?php

namespace jamesvweston\Stripe;


use Dotenv\Dotenv;
use jamesvweston\Stripe\Api\AccountApi;
use jamesvweston\Stripe\Api\BalanceApi;
use jamesvweston\Stripe\Api\ChargeApi;
use jamesvweston\Stripe\Api\CustomerApi;
use jamesvweston\Stripe\Api\RefundApi;
use jamesvweston\Stripe\Api\TransferApi;
use jamesvweston\Utilities\ArrayUtil AS AU;


class StripeClient
{

    /**
     * @var     StripeApiConfiguration
     */
    protected $stripeApiConfiguration;

    /**
     * @var AccountApi
     */
    public $accountApi;

    /**
     * @var BalanceApi
     */
    public $balanceApi;

    /**
     * @var ChargeApi
     */
    public $chargeApi;
    
    /**
     * @var CustomerApi
     */
    public $customerApi;

    /**
     * @var RefundApi
     */
    public $refundApi;

    /**
     * @var TransferApi
     */
    public $transferApi;

    
    public function __construct($config)
    {
        if (is_string($config))
        {
            if (!is_dir($config))
                echo 'fix this';
            //  throw new InvalidConfigurationException('The provided directory location does not exist at ' . $config, 400);

            $dotEnv                         = new Dotenv($config);
            $dotEnv->load();

            $data = [
                'secret'                    => getenv('STRIPE_SECRET'),
                'version'                   => getenv('STRIPE_VERSION'),
            ];
        } else {
            if (is_array($config))
            {
                $data = [
                    'secret'                => AU::get($config['secret']),
                    'version'               => AU::get($config['version']),
                ];
            } else {
                throw new \InvalidArgumentException('A configuration must be provided');
            }
        }
        
        $this->stripeApiConfiguration       = new StripeApiConfiguration($data);
        $this->accountApi                   = new AccountApi($this->stripeApiConfiguration);
        $this->balanceApi                   = new BalanceApi($this->stripeApiConfiguration);
        $this->chargeApi                    = new ChargeApi($this->stripeApiConfiguration);
        $this->customerApi                  = new CustomerApi($this->stripeApiConfiguration);
        $this->refundApi                    = new RefundApi($this->stripeApiConfiguration);
        $this->transferApi                  = new TransferApi($this->stripeApiConfiguration);
    }


    /**
     * @return StripeApiConfiguration
     */
    public function getStripeApiConfiguration()
    {
        return $this->stripeApiConfiguration;
    }

    /**
     * @param StripeApiConfiguration $stripeApiConfiguration
     */
    public function setStripeApiConfiguration($stripeApiConfiguration)
    {
        $this->stripeApiConfiguration = $stripeApiConfiguration;
    }
    
}