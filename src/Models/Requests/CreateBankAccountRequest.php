<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateBankAccountRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateBankAccountRequest extends BaseCreateBankAccountRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            if (!is_null(AU::get($data['source'])))
                $this->source               = new CreateBankAccountSourceRequest(AU::get($data['source']));

            if (!is_null(AU::get($data['external_account'])))
                $this->external_account     = new CreateBankAccountSourceRequest(AU::get($data['external_account']));

            $this->metadata                 = AU::get($data['metadata'], []);
            $this->default_for_currency     = AU::get($data['default_for_currency']);
        }
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['source']                   = ($this->source instanceof \JsonSerializable) ? $this->source->jsonSerialize() : null;
        $object['external_account']         = ($this->external_account instanceof \JsonSerializable) ? $this->external_account->jsonSerialize() : null;
        $object['metadata']                 = $this->metadata;
        $object['default_for_currency']     = $this->default_for_currency;

        return $object;
    }

}