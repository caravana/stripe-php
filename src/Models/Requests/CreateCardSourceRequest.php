<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateCardSourceRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateCardSourceRequest extends BaseCreateCardSourceRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->object                   = 'card';
            $this->exp_month                = AU::get($data['exp_month']);
            $this->exp_year                 = AU::get($data['exp_year']);
            $this->number                   = AU::get($data['number']);
            $this->address_city             = AU::get($data['address_city']);
            $this->address_country          = AU::get($data['address_country']);
            $this->address_line1            = AU::get($data['address_line1']);
            $this->address_line2            = AU::get($data['address_line2']);
            $this->address_state            = AU::get($data['address_state']);
            $this->address_zip              = AU::get($data['address_zip']);
            $this->currency                 = AU::get($data['currency']);
            $this->cvc                      = AU::get($data['cvc']);
            $this->default_for_currency     = AU::get($data['default_for_currency']);
            $this->metadata                 = AU::get($data['metadata'], []);
            $this->name                     = AU::get($data['name']);
        }
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['object']                   = $this->object;
        $object['exp_month']                = $this->exp_month;
        $object['exp_year']                 = $this->exp_year;
        $object['number']                   = $this->number;
        $object['address_city']             = $this->address_city;
        $object['address_country']          = $this->address_country;
        $object['address_line1']            = $this->address_line1;
        $object['address_line2']            = $this->address_line2;
        $object['address_state']            = $this->address_state;
        $object['address_zip']              = $this->address_zip;
        $object['currency']                 = $this->currency;
        $object['cvc']                      = $this->cvc;
        $object['default_for_currency']     = $this->default_for_currency;
        $object['metadata']                 = $this->metadata;
        $object['name']                     = $this->name;

        return $object;
    }
}