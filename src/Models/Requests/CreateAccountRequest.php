<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateAccountRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateAccountRequest extends BaseCreateAccountRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->country                  = AU::get($data['country']);
            $this->email                    = AU::get($data['email']);
            $this->managed                  = AU::get($data['managed']);
        }
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['country']                  = $this->country;
        $object['email']                    = $this->email;
        $object['managed']                  = $this->managed;

        return $object;
    }

}