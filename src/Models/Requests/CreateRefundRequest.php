<?php



namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateRefundRequest;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateRefundRequest extends BaseCreateRefundRequest
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->charge                   = AU::get($data['charge']);
            $this->amount                   = AU::get($data['amount']);
            $this->metadata                 = AU::get($data['metadata']);
            $this->reason                   = AU::get($data['reason']);
            $this->refund_application_fee   = AU::get($data['refund_application_fee']);
            $this->reverse_transfer         = AU::get($data['reverse_transfer']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['charge']                   = $this->charge;
        $object['amount']                   = $this->amount;
        $object['metadata']                 = $this->metadata;
        $object['reason']                   = $this->reason;
        $object['refund_application_fee']   = $this->refund_application_fee;
        $object['reverse_transfer']         = $this->reverse_transfer;

        return $object;
    }
    
}