<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface CreateAddressRequest extends \JsonSerializable
{
    function getLine1();
    function setLine1($line1);
    function getCity();
    function setCity($city);
    function getCountry();
    function setCountry($country);
    function getLine2();
    function setLine2($line2);
    function getPostalCode();
    function setPostalCode($postal_code);
    function getState();
    function setState($state);
}