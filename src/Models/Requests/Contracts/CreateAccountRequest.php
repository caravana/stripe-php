<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface CreateAccountRequest extends \JsonSerializable
{
    function getCountry();
    function setCountry($country);
    function getEmail();
    function setEmail($email);
    function getManaged();
    function setManaged($managed);
}