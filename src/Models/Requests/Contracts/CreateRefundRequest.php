<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;

interface CreateRefundRequest extends \JsonSerializable
{
    public function getCharge();

    public function setCharge($charge);

    public function getAmount();

    public function setAmount($amount);

    public function getMetadata();

    public function setMetadata($metadata);

    public function getReason();

    public function setReason($reason);

    public function getRefundApplicationFee();

    public function setRefundApplicationFee($refund_application_fee);

    public function getReverseTransfer();

    public function setReverseTransfer($reverse_transfer);
}