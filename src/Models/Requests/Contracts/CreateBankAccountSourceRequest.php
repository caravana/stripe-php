<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface CreateBankAccountSourceRequest extends \JsonSerializable
{
    function getObject();
    function setObject($object);
    function getAccountNumber();
    function setAccountNumber($account_number);
    function getCountry();
    function setCountry($country);
    function getCurrency();
    function setCurrency($currency);
    function getAccountHolderName();
    function setAccountHolderName($account_holder_name);
    function getAccountHolderType();
    function setAccountHolderType($account_holder_type);
    function getRoutingNumber();
    function setRoutingNumber($routing_number);
}