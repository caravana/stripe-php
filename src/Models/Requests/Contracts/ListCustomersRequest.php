<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface ListCustomersRequest extends \JsonSerializable
{
    function getCreated();
    function setCreated($created);
    function getEndingBefore();
    function setEndingBefore($ending_before);
    function getLimit();
    function setLimit($limit);
    function getStartingAfter();
    function setStartingAfter($starting_after);
}