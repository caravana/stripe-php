<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface CreateShippingInfoRequest extends \JsonSerializable
{
    function getAddress();
    function setAddress($address);
    function getName();
    function setName($name);
    function getPhone();
    function setPhone($phone);
}