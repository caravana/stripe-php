<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface CreateCardRequest extends  \JsonSerializable
{
    function getSource();
    function setSource($source);
    function getExternalAccount();
    function setExternalAccount($external_account);
    function getMetadata();
    function setMetadata($metadata);
    function getDefaultForCurrency();
    function setDefaultForCurrency($default_for_currency);
}