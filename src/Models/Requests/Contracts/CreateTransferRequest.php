<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;

interface CreateTransferRequest extends \JsonSerializable
{
    public function getAmount();

    public function setAmount($amount);

    public function getApplicationFee();

    public function setApplicationFee($application_fee);

    public function getCurrency();

    public function setCurrency($currency);

    public function getDestination();

    public function setDestination($destination);

    public function getDescription();

    public function setDescription($description);

    public function getMetadata();

    public function setMetadata($metadata);

    public function getSourceTransaction();

    public function setSourceTransaction($source_transaction);

    public function getStatementDescriptor();

    public function setStatementDescriptor($statement_descriptor);

    public function getSourceType();

    public function setSourceType($source_type);

    public function getMethod();

    public function setMethod($method);

}