<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface CreateChargeRequest extends \JsonSerializable
{
    function getAmount();
    function setAmount($amount);
    function getCurrency();
    function setCurrency($currency);
    function getApplicationFee();
    function setApplicationFee($application_fee);
    function getCapture();
    function setCapture($capture);
    function getDescription();
    function setDescription($description);
    function getDestination();
    function setDestination($destination);
    function getMetadata();
    function setMetadata($metadata);
    function getReceiptEmail();
    function setReceiptEmail($receipt_email);
    function getShipping();
    function setShipping($shipping);
    function getCustomer();
    function setCustomer($customer);
    function getSource();
    function setSource($source);
    function getStatementDescriptor();
    function setStatementDescriptor($statement_descriptor);
}