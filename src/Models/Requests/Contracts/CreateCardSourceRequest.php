<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface CreateCardSourceRequest extends \JsonSerializable
{
    function getObject();
    function setObject($object);
    function getExpMonth();
    function setExpMonth($exp_month);
    function getExpYear();
    function setExpYear($exp_year);
    function getNumber();
    function setNumber($number);
    function getAddressCity();
    function setAddressCity($address_city);
    function getAddressCountry();
    function setAddressCountry($address_country);
    function getAddressLine1();
    function setAddressLine1($address_line1);
    function getAddressLine2();
    function setAddressLine2($address_line2);
    function getAddressState();
    function setAddressState($address_state);
    function getAddressZip();
    function setAddressZip($address_zip);
    function getCurrency();
    function setCurrency($currency);
    function getCvc();
    function setCvc($cvc);
    function getDefaultForCurrency();
    function setDefaultForCurrency($default_for_currency);
    function getMetadata();
    function setMetadata($metadata);
    function getName();
    function setName($name);
}