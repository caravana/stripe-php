<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface Validatable
{
    function validate();
}