<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface Created extends \JsonSerializable
{
    function getGt();
    function setGt($gt);
    function getGte();
    function setGte($gte);
    function getLt();
    function setLt($lt);
    function getLte();
    function setLte($lte);
}