<?php

namespace jamesvweston\Stripe\Models\Requests\Contracts;


interface CreateBankAccountRequest extends \JsonSerializable
{
    function getSource();
    function setSource($source);
    function getExternalAccount();
    function setExternalAccount($external_account);
    function getDefaultForCurrency();
    function setDefaultForCurrency($default_for_currency);
    function getMetadata();
    function setMetadata($metadata);
}