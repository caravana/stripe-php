<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateChargeRequest AS CreateChargeRequestContract;

/**
 * @see https://stripe.com/docs/api#create_charge
 * Class BaseCreateChargeRequest
 * @package jamesvweston\Stripe\Models\Requests\Base
 */
abstract class BaseCreateChargeRequest implements CreateChargeRequestContract
{

    /**
     * A positive integer in the smallest currency unit
     * e.g. $1.00 = 100
     * The minimum amount is $0.50 US
     * @var int
     */
    protected $amount;

    /**
     * 3-letter ISO code for currency
     * @var string
     */
    protected $currency;

    /**
     * CONNECT ONLY
     * A fee in cents that will be applied to the charge and transferred to the application owner's Stripe account
     * @var int|null
     */
    protected $application_fee;


    /**
     * Default is true
     * Whether or not to immediately capture the charge
     * When false, the charge issues an authorization (or pre-authorization), and will need to be captured late
     * @var bool|null
     */
    protected $capture;

    /**
     * An arbitrary string which you can attach to a charge object
     * @var string|null
     */
    protected $description;

    /**
     * CONNECT ONLY
     * An account to make the charge on behalf of
     * If specified, the charge will be attributed to the destination account for tax reporting, and the funds from the charge will be transferred to the destination account
     * @var string
     */
    protected $destination;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * The email address to send this charge's receipt to
     * @var string|null
     */
    protected $receipt_email;

    /**
     * @var array|null
     */
    protected $shipping;

    /**
     * Optional, either customer or source is required
     * The ID of an existing customer that will be charged in this request
     * @var string|null
     */
    protected $customer;

    /**
     * Optional, either source or customer is required
     * A payment source to be charged, such as a credit card
     * If you also pass a customer ID, the source must be the ID of a source belonging to the customer
     * @var string|null
     */
    protected $source;

    /**
     * An arbitrary string to be displayed on your customer's credit card statement
     * This may be up to 22 characters
     * @var string|null
     */
    protected $statement_descriptor;

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return int|null
     */
    public function getApplicationFee()
    {
        return $this->application_fee;
    }

    /**
     * @param int|null $application_fee
     */
    public function setApplicationFee($application_fee)
    {
        $this->application_fee = $application_fee;
    }

    /**
     * @return bool|null
     */
    public function getCapture()
    {
        return $this->capture;
    }

    /**
     * @param bool|null $capture
     */
    public function setCapture($capture)
    {
        $this->capture = $capture;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return null|string
     */
    public function getReceiptEmail()
    {
        return $this->receipt_email;
    }

    /**
     * @param null|string $receipt_email
     */
    public function setReceiptEmail($receipt_email)
    {
        $this->receipt_email = $receipt_email;
    }

    /**
     * @return array|null
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param array|null $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return null|string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param null|string $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return null|string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param null|string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return null|string
     */
    public function getStatementDescriptor()
    {
        return $this->statement_descriptor;
    }

    /**
     * @param null|string $statement_descriptor
     */
    public function setStatementDescriptor($statement_descriptor)
    {
        $this->statement_descriptor = $statement_descriptor;
    }
    
}