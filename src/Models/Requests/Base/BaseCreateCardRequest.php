<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateCardRequest AS CreateCardRequestContract;

/**
 * Class BaseCreateCardRequest
 * @see     https://stripe.com/docs/api#create_card
 * @package jamesvweston\Stripe\Models\Requests\Base
 */
abstract class BaseCreateCardRequest implements CreateCardRequestContract
{

    /**
     * When adding a card to a customer, the parameter name is source
     * @var BaseCreateCardSourceRequest|null
     */
    protected $source;

    /**
     * When adding to an account, the parameter name is external_account
     * @var BaseCreateCardSourceRequest|null
     */
    protected $external_account;

    /**
     * A set of key/value pairs that you can attach to a card object.
     * It can be useful for storing additional information about the card in a structured format
     * @var array
     */
    protected $metadata;


    /**
     * MANAGED ACCOUNTS ONLY
     * Only applicable on accounts (not customers or recipients).
     * If you set this to true (or if this is the first external account being added in this currency) this card will become the default external account for its currency.
     * @var bool|null
     */
    protected $default_for_currency;

    /**
     * @return BaseCreateCardSourceRequest|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param BaseCreateCardSourceRequest|null $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return BaseCreateCardSourceRequest|null
     */
    public function getExternalAccount()
    {
        return $this->external_account;
    }

    /**
     * @param BaseCreateCardSourceRequest|null $external_account
     */
    public function setExternalAccount($external_account)
    {
        $this->external_account = $external_account;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return bool|null
     */
    public function getDefaultForCurrency()
    {
        return $this->default_for_currency;
    }

    /**
     * @param bool|null $default_for_currency
     */
    public function setDefaultForCurrency($default_for_currency)
    {
        $this->default_for_currency = $default_for_currency;
    }

}