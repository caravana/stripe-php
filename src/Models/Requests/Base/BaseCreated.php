<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\Created AS CreatedContract;

abstract class BaseCreated implements CreatedContract
{

    /**
     * Return values where the created field is after this timestamp.
     * @var int|null
     */
    protected $gt;

    /**
     * Return values where the created field is after or equal to this timestamp.
     * @var int|null
     */
    protected $gte;

    /**
     * Return values where the created field is before this timestamp.
     * @var int|null
     */
    protected $lt;

    /**
     * Return values where the created field is before or equal to this timestamp.
     * @var int|null
     */
    protected $lte;

    /**
     * @return int|null
     */
    public function getGt()
    {
        return $this->gt;
    }

    /**
     * @param int|null $gt
     */
    public function setGt($gt)
    {
        $this->gt = $gt;
    }

    /**
     * @return int|null
     */
    public function getGte()
    {
        return $this->gte;
    }

    /**
     * @param int|null $gte
     */
    public function setGte($gte)
    {
        $this->gte = $gte;
    }

    /**
     * @return int|null
     */
    public function getLt()
    {
        return $this->lt;
    }

    /**
     * @param int|null $lt
     */
    public function setLt($lt)
    {
        $this->lt = $lt;
    }

    /**
     * @return int|null
     */
    public function getLte()
    {
        return $this->lte;
    }

    /**
     * @param int|null $lte
     */
    public function setLte($lte)
    {
        $this->lte = $lte;
    }
    
}