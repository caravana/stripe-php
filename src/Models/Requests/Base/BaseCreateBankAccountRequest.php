<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateBankAccountRequest AS CreateBankAccountRequestContract;

abstract class BaseCreateBankAccountRequest implements CreateBankAccountRequestContract
{

    /**
     * When adding a bank account to a customer, the parameter name is source
     * @var BaseCreateBankAccountSourceRequest|null
     */
    protected $source;

    /**
     * When adding to an account, the parameter name is external_account
     * @var BaseCreateBankAccountSourceRequest|null
     */
    protected $external_account;

    /**
     * f you set this to true (or if this is the first bank account being added in
     * this currency) this bank account will become the default bank account for its currency.
     * @var bool|null
     */
    protected $default_for_currency;

    /**
     * A set of key/value pairs that you can attach to an external account object. 
     * It can be useful for storing additional information about the external account in a structured format.
     * @var array|null
     */
    protected $metadata;

    /**
     * @return BaseCreateBankAccountSourceRequest|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param BaseCreateBankAccountSourceRequest|null $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return BaseCreateBankAccountSourceRequest|null
     */
    public function getExternalAccount()
    {
        return $this->external_account;
    }

    /**
     * @param BaseCreateBankAccountSourceRequest|null $external_account
     */
    public function setExternalAccount($external_account)
    {
        $this->external_account = $external_account;
    }

    /**
     * @return bool|null
     */
    public function getDefaultForCurrency()
    {
        return $this->default_for_currency;
    }

    /**
     * @param bool|null $default_for_currency
     */
    public function setDefaultForCurrency($default_for_currency)
    {
        $this->default_for_currency = $default_for_currency;
    }

    /**
     * @return array|null
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array|null $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }
    
}