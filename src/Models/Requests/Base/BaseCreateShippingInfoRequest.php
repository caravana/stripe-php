<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateShippingInfoRequest AS CreateShippingInfoRequestContract;

abstract class BaseCreateShippingInfoRequest implements CreateShippingInfoRequestContract
{

    /**
     * @var BaseCreateAddressRequest
     */
    protected $address;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $phone;

    /**
     * @return BaseCreateAddressRequest
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param BaseCreateAddressRequest $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
}