<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateBankAccountSourceRequest AS CreateBankAccountSourceRequestContract;

abstract class BaseCreateBankAccountSourceRequest implements CreateBankAccountSourceRequestContract
{

    /**
     * @var string
     */
    protected $object   = 'bank_account';

    /**
     * The account number for the bank account in string form.
     * Must be a checking account
     * @var string
     */
    protected $account_number;

    /**
     * The country the bank account is in
     * @var string
     */
    protected $country;

    /**
     * The currency the bank account is in
     * @var string
     */
    protected $currency;

    /**
     * The name of the person or business that owns the bank account.
     * This field is required when attaching the bank account to a customer object.
     * @var string
     */
    protected $account_holder_name;

    /**
     * The type of entity that holds the account.
     * This can be either "individual" or "company".
     * This field is required when attaching the bank account to a customer object
     * @var string
     */
    protected $account_holder_type;

    /**
     * @var string
     */
    protected $routing_number;

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->account_number;
    }

    /**
     * @param string $account_number
     */
    public function setAccountNumber($account_number)
    {
        $this->account_number = $account_number;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getAccountHolderName()
    {
        return $this->account_holder_name;
    }

    /**
     * @param string $account_holder_name
     */
    public function setAccountHolderName($account_holder_name)
    {
        $this->account_holder_name = $account_holder_name;
    }

    /**
     * @return string
     */
    public function getAccountHolderType()
    {
        return $this->account_holder_type;
    }

    /**
     * @param string $account_holder_type
     */
    public function setAccountHolderType($account_holder_type)
    {
        $this->account_holder_type = $account_holder_type;
    }

    /**
     * @return string
     */
    public function getRoutingNumber()
    {
        return $this->routing_number;
    }

    /**
     * @param string $routing_number
     */
    public function setRoutingNumber($routing_number)
    {
        $this->routing_number = $routing_number;
    }

}