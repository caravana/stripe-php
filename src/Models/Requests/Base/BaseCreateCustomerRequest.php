<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateCustomerRequest AS CreateCustomerRequestContract;

/**
 * Class BaseCreateCustomerRequest
 * @see https://stripe.com/docs/api#create_customer
 * @package jamesvweston\Stripe\Models\Requests\Base
 */
abstract class BaseCreateCustomerRequest implements CreateCustomerRequestContract
{

    /**
     * An integer amount in cents that is the starting account balance for your customer.
     * A negative amount represents a credit that will be used before attempting any charges to the customer’s card.
     * A positive amount will be added to the next invoice.
     * @var     float|null
     */
    protected $account_balance;

    /**
     * The customer’s VAT identification number.
     * If you are using Relay, this field gets passed to tax provider you are using for your orders.
     * This will be unset if you POST an empty value.
     * This can be unset by updating the value to null and then saving.
     * @var     string|null
     */
    protected $business_vat_id;

    /**
     * If you provide a coupon code, the customer will have a discount applied on all recurring charges.
     * Charges you create through the API will not have the discount.
     * @var string|null
     */
    protected $coupon;

    /**
     * An arbitrary string that you can attach to a customer object.
     * It is displayed alongside the customer in the dashboard. This will be unset if you POST an empty value.
     * This can be unset by updating the value to null and then saving.
     * @var     string|null
     */
    protected $description;

    /**
     * Customer’s email address.
     * It’s displayed alongside the customer in your dashboard and can be useful for searching and tracking.
     * This will be unset if you POST an empty value.This can be unset by updating the value to null and then saving.
     * @var     string|null
     */
    protected $email;

    /**
     * A set of key/value pairs that you can attach to a customer object.
     * It can be useful for storing additional information about the customer in a structured format.
     * This will be unset if you POST an empty value.This can be unset by updating the value to null and then saving.
     * @var     array
     */
    protected $metadata;

    /**
     * The identifier of the plan to subscribe the customer to.
     * If provided, the returned customer object will have a list of subscriptions that the customer is currently subscribed to.
     * If you subscribe a customer to a plan without a free trial, the customer must have a valid card as well.
     * @var string|null
     */
    protected $plan;

    /**
     * The quantity you’d like to apply to the subscription you’re creating (if you pass in a plan).
     * For example, if your plan is 10 cents/user/month, and your customer has 5 users, you could pass 5 as the quantity to have the customer charged 50 cents (5 x 10 cents) monthly.
     * Defaults to 1 if not set. Only applies when the plan parameter is also provided.
     * @var int|null
     */
    protected $quantity;

    /**
     * @var BaseCreateShippingInfoRequest|null
     */
    protected $shipping;

    //  TODO: Implement source
    protected $source;

    /**
     * A positive decimal (with at most two decimal places) between 1 and 100.
     * This represents the percentage of the subscription invoice subtotal that will be calculated and added as tax to the final amount each billing period.
     * For example, a plan which charges $10/month with a tax_percent of 20.0 will charge $12 per invoice.
     * Can only be used if a plan is provided.
     * @var float|null
     */
    protected $tax_percent;

    /**
     * Unix timestamp representing the end of the trial period the customer will get before being charged.
     * If set, trial_end will override the default trial period of the plan the customer is being subscribed to.
     * The special value now can be provided to end the customer’s trial immediately.
     * Only applies when the plan parameter is also provided.
     * @var int|null
     */
    protected $trial_end;

    /**
     * @return float|null
     */
    public function getAccountBalance()
    {
        return $this->account_balance;
    }

    /**
     * @param float|null $account_balance
     */
    public function setAccountBalance($account_balance)
    {
        $this->account_balance = $account_balance;
    }

    /**
     * @return null|string
     */
    public function getBusinessVatId()
    {
        return $this->business_vat_id;
    }

    /**
     * @param null|string $business_vat_id
     */
    public function setBusinessVatId($business_vat_id)
    {
        $this->business_vat_id = $business_vat_id;
    }

    /**
     * @return null|string
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param null|string $coupon
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return null|string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param null|string $plan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;
    }

    /**
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return BaseCreateShippingInfoRequest|null
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param BaseCreateShippingInfoRequest|null $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return float|null
     */
    public function getTaxPercent()
    {
        return $this->tax_percent;
    }

    /**
     * @param float|null $tax_percent
     */
    public function setTaxPercent($tax_percent)
    {
        $this->tax_percent = $tax_percent;
    }

    /**
     * @return int|null
     */
    public function getTrialEnd()
    {
        return $this->trial_end;
    }

    /**
     * @param int|null $trial_end
     */
    public function setTrialEnd($trial_end)
    {
        $this->trial_end = $trial_end;
    }
    
}