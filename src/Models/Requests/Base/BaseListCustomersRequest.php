<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\ListCustomersRequest AS ListCustomersRequestContract;

/**
 * Class BaseListCustomersRequest
 * @see https://stripe.com/docs/api#list_customers
 * @package jamesvweston\Stripe\Models\Requests\Base
 */
abstract class BaseListCustomersRequest implements ListCustomersRequestContract
{

    /**
     * @var BaseCreated|null
     */
    protected $created;

    /**
     * A cursor for use in pagination.
     * ending_before is an object ID that defines your place in the list.
     * For instance, if you make a list request and receive 100 objects, starting with obj_bar, your subsequent call can include ending_before=obj_bar in order to fetch the previous page of the list.
     * @var string|null
     */
    protected $ending_before;

    /**
     * A limit on the number of objects to be returned.
     * Limit can range between 1 and 100 items.
     * @var int|null
     */
    protected $limit;

    /**
     * A cursor for use in pagination. starting_after is an object ID that defines your place in the list.
     * For instance, if you make a list request and receive 100 objects, ending with obj_foo, your subsequent call can include starting_after=obj_foo in order to fetch the next page of the list.
     * @var string|null
     */
    protected $starting_after;

    /**
     * @return BaseCreated|null
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param BaseCreated|null $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return null|string
     */
    public function getEndingBefore()
    {
        return $this->ending_before;
    }

    /**
     * @param null|string $ending_before
     */
    public function setEndingBefore($ending_before)
    {
        $this->ending_before = $ending_before;
    }

    /**
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return null|string
     */
    public function getStartingAfter()
    {
        return $this->starting_after;
    }

    /**
     * @param null|string $starting_after
     */
    public function setStartingAfter($starting_after)
    {
        $this->starting_after = $starting_after;
    }

}