<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateAccountRequest AS CreateAccountRequestContract;

/**
 * @see https://stripe.com/docs/api#create_account
 * Class BaseCreateAccountRequest
 * @package jamesvweston\Stripe\Models\Requests\Base
 */
abstract class BaseCreateAccountRequest implements CreateAccountRequestContract
{

    /**
     * The country the account holder resides in or that the business is legally established in
     * Default is your own country
     * @var string|null
     */
    protected $country;

    /**
     * For standalone accounts, Stripe will email your user with instructions for how to set up their account
     * For managed accounts, this is only to make the account easier to identify to you
     * @var string|null
     */
    protected $email;

    /**
     * Default is false
     * @var bool
     */
    protected $managed;

    /**
     * @return null|string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param null|string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return boolean
     */
    public function getManaged()
    {
        return $this->managed;
    }

    /**
     * @param boolean $managed
     */
    public function setManaged($managed)
    {
        $this->managed = $managed;
    }
}