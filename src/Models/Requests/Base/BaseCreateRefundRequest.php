<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateRefundRequest AS CreateRefundRequestContract;

abstract class BaseCreateRefundRequest implements CreateRefundRequestContract
{

    /**
     * The identifier of the charge to refund
     * @var string
     */
    protected $charge;

    /**
     * A positive integer in cents representing how much of this charge to refund
     * @var int
     */
    protected $amount;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * String indicating the reason for the refund
     * If set, possible values are duplicate, fraudulent, and requested_by_customer
     * @var string|null
     */
    protected $reason;

    /**
     * CONNECT ONLY
     * Boolean indicating whether the application fee should be refunded when refunding this charge
     * @var bool|null
     */
    protected $refund_application_fee;

    /**
     * CONNECT ONLY
     * Boolean indicating whether the transfer should be reversed when refunding this charge
     * @var bool|null
     */
    protected $reverse_transfer;

    /**
     * @return string
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * @param string $charge
     */
    public function setCharge($charge)
    {
        $this->charge = $charge;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return null|string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param null|string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return bool|null
     */
    public function getRefundApplicationFee()
    {
        return $this->refund_application_fee;
    }

    /**
     * @param bool|null $refund_application_fee
     */
    public function setRefundApplicationFee($refund_application_fee)
    {
        $this->refund_application_fee = $refund_application_fee;
    }

    /**
     * @return bool|null
     */
    public function getReverseTransfer()
    {
        return $this->reverse_transfer;
    }

    /**
     * @param bool|null $reverse_transfer
     */
    public function setReverseTransfer($reverse_transfer)
    {
        $this->reverse_transfer = $reverse_transfer;
    }
    
}