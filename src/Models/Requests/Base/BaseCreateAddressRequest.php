<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateAddressRequest AS CreateAddressRequestContract;

/**
 * Class BaseCreateAddressRequest
 * @package jamesvweston\Stripe\Models\Requests\Base
 */
abstract class BaseCreateAddressRequest implements CreateAddressRequestContract
{

    /**
     * @var string
     */
    protected $line1;

    /**
     * @var string|null
     */
    protected $city;

    /**
     * @var string|null
     */
    protected $country;

    /**
     * @var string|null
     */
    protected $line2;

    /**
     * @var string|null
     */
    protected $postal_code;

    /**
     * @var string|null
     */
    protected $state;

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param string $line1
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;
    }

    /**
     * @return null|string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param null|string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return null|string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param null|string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return null|string
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param null|string $line2
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;
    }

    /**
     * @return null|string
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param null|string $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /**
     * @return null|string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param null|string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }
    
}