<?php

namespace jamesvweston\Stripe\Models\Requests\Base;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateTransferRequest AS CreateTransferRequestContract;

abstract class BaseCreateTransferRequest implements CreateTransferRequestContract
{

    /**
     * A positive integer in cents representing how much to transfe
     * @var int
     */
    protected $amount;

    /**
     * A fee in cents that will be applied to the transfer and transferred to the application owner's Stripe account
     * @var int|null
     */
    protected $application_fee;
    /**
     * 3-letter ISO code for currency
     * @var string
     */
    protected $currency;

    /**
     * The id of a bank account or a card to send the transfer to,
     * or the string default_for_currency to use the default external account for the specified currency
     * @var string
     */
    protected $destination;

    /**
     * An arbitrary string which you can attach to a transfer object
     * It is displayed when in the web interface alongside the transfer
     * @var string|null
     */
    protected $description;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * A string to be displayed on the recipient's bank or card statement
     * This may be at most 22 characters
     * @var string|null
     */
    protected $source_transaction;

    /**
     * A string to be displayed on the recipient's bank or card statement.
     * This may be at most 22 characters.
     * @var string|null
     */
    protected $statement_descriptor;

    /**
     * The source balance to draw this transfer from
     * options are: alipay_account, bank_account, bitcoin_receiver, and card
     * @var string|null
     */
    protected $source_type;

    /**
     * The method used to send this transfer, which can be standard or instant
     * Default is instant
     * @var string|null
     */
    protected $method;

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int|null
     */
    public function getApplicationFee()
    {
        return $this->application_fee;
    }

    /**
     * @param int|null $application_fee
     */
    public function setApplicationFee($application_fee)
    {
        $this->application_fee = $application_fee;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return null|string
     */
    public function getSourceTransaction()
    {
        return $this->source_transaction;
    }

    /**
     * @param null|string $source_transaction
     */
    public function setSourceTransaction($source_transaction)
    {
        $this->source_transaction = $source_transaction;
    }

    /**
     * @return null|string
     */
    public function getStatementDescriptor()
    {
        return $this->statement_descriptor;
    }

    /**
     * @param null|string $statement_descriptor
     */
    public function setStatementDescriptor($statement_descriptor)
    {
        $this->statement_descriptor = $statement_descriptor;
    }

    /**
     * @return null|string
     */
    public function getSourceType()
    {
        return $this->source_type;
    }

    /**
     * @param null|string $source_type
     */
    public function setSourceType($source_type)
    {
        $this->source_type = $source_type;
    }

    /**
     * @return null|string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param null|string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }
    
}