<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreated;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Created extends BaseCreated
{

    /**
     * Created constructor.
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->gt                       = AU::get($data['gt']);
            $this->gte                      = AU::get($data['gte']);
            $this->lt                       = AU::get($data['lt']);
            $this->lte                      = AU::get($data['lte']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['gt']                       = $this->gt;
        $object['gte']                      = $this->gte;
        $object['lt']                       = $this->lt;
        $object['lte']                      = $this->lte;

        return $object;
    }
}