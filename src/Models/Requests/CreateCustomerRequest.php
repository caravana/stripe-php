<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateCustomerRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateCustomerRequest extends BaseCreateCustomerRequest implements Validatable
{

    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->account_balance          = AU::get($data['account_balance']);
            $this->business_vat_id          = AU::get($data['business_vat_id']);
            $this->coupon                   = AU::get($data['coupon']);
            $this->description              = AU::get($data['description']);
            $this->email                    = AU::get($data['email']);
            $this->metadata                 = AU::get($data['metadata'], []);
            $this->plan                     = AU::get($data['plan']);
            $this->quantity                 = AU::get($data['quantity']);
            $this->shipping                 = AU::get($data['shipping']);
            $this->source                   = AU::get($data['source']);
            $this->tax_percent              = AU::get($data['tax_percent']);
            $this->trial_end                = AU::get($data['trial_end']);
        }
    }
    
    public function validate()
    {
        // TODO: Implement validate() method.
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        //  TODO: Implement source

        $object['account_balance']          = $this->account_balance;
        $object['business_vat_id']          = $this->business_vat_id;
        $object['coupon']                   = $this->coupon;
        $object['description']              = $this->description;
        $object['email']                    = $this->email;
        $object['metadata']                 = $this->metadata;
        $object['plan']                     = $this->plan;
        $object['quantity']                 = $this->quantity;
        $object['shipping']                 = ($this->shipping instanceof \JsonSerializable) ? $this->shipping->jsonSerialize() : null;
        $object['source']                   = ($this->source instanceof \JsonSerializable) ? $this->source->jsonSerialize() : null;
        $object['tax_percent']              = $this->tax_percent;
        $object['trial_end']                = $this->trial_end;

        return $object;
    }
}