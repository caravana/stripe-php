<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateBankAccountSourceRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateBankAccountSourceRequest extends BaseCreateBankAccountSourceRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->account_number           = AU::get($data['account_number']);
            $this->country                  = AU::get($data['country']);
            $this->currency                 = AU::get($data['currency']);
            $this->account_holder_name      = AU::get($data['account_holder_name']);
            $this->account_holder_type      = AU::get($data['account_holder_type']);
            $this->routing_number           = AU::get($data['routing_number']);
        }
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['object']                   = $this->object;
        $object['account_number']           = $this->account_number;
        $object['country']                  = $this->country;
        $object['currency']                 = $this->currency;
        $object['account_holder_name']      = $this->account_holder_name;
        $object['account_holder_type']      = $this->account_holder_type;
        $object['routing_number']           = $this->routing_number;

        return $object;
    }
    
}