<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateChargeRequest;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateChargeRequest extends BaseCreateChargeRequest
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->amount                   = AU::get($data['amount']);
            $this->currency                 = AU::get($data['currency']);
            $this->application_fee          = AU::get($data['application_fee']);
            $this->capture                  = AU::get($data['capture']);
            $this->description              = AU::get($data['description']);
            $this->destination              = AU::get($data['destination']);
            $this->metadata                 = AU::get($data['metadata']);
            $this->receipt_email            = AU::get($data['receipt_email']);
            $this->shipping                 = AU::get($data['shipping']);
            $this->customer                 = AU::get($data['customer']);
            $this->source                   = AU::get($data['source']);
            $this->statement_descriptor     = AU::get($data['statement_descriptor']);
        }
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['amount']                   = $this->amount;
        $object['currency']                 = $this->currency;
        $object['application_fee']          = $this->application_fee;
        $object['capture']                  = $this->capture;
        $object['description']              = $this->description;
        $object['destination']              = $this->destination;
        $object['metadata']                 = $this->metadata;
        $object['receipt_email']            = $this->receipt_email;
        $object['shipping']                 = $this->shipping;
        $object['customer']                 = $this->customer;
        $object['source']                   = $this->source;
        $object['statement_descriptor']     = $this->statement_descriptor;

        return $object;
    }
    
}