<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseListCustomersRequest;
use jamesvweston\Utilities\ArrayUtil AS AU;

class ListCustomersRequest extends BaseListCustomersRequest
{

    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->created                  = AU::get($data['created']);
            $this->ending_before            = AU::get($data['ending_before']);
            $this->limit                    = AU::get($data['limit']);
            $this->starting_after           = AU::get($data['starting_after']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['created']                  = ($this->created instanceof \JsonSerializable) ? $this->created->jsonSerialize() : null;
        $object['ending_before']            = $this->ending_before;
        $object['limit']                    = $this->limit;
        $object['starting_after']           = $this->starting_after;

        return $object;
    }
    
}