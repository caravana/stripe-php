<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateShippingInfoRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateShippingInfoRequest extends BaseCreateShippingInfoRequest implements Validatable
{

    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->address                  = AU::get($data['address']);
            $this->name                     = AU::get($data['name']);
            $this->phone                    = AU::get($data['phone']);
        }
    }

    public function validate()
    {
        if ($this->address instanceof Validatable)
            $this->address->validate();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['address']                  = $this->address instanceof \JsonSerializable ? $this->address->jsonSerialize() : null;
        $object['name']                     = $this->name;
        $object['phone']                    = $this->phone;
        
        return $object;
    }
    
}