<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateAddressRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateAddressRequest extends BaseCreateAddressRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->line1                    = AU::get($data['line1']);
            $this->city                     = AU::get($data['city']);
            $this->country                  = AU::get($data['country']);
            $this->line2                    = AU::get($data['line2']);
            $this->postal_code              = AU::get($data['postal_code']);
            $this->state                    = AU::get($data['state']);
        }
    }
    
    public function validate()
    {
        // TODO: Implement validate() method.
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['line1']                    = $this->line1;
        $object['city']                     = $this->city;
        $object['country']                  = $this->country;
        $object['line2']                    = $this->line2;
        $object['postal_code']              = $this->postal_code;
        $object['state']                    = $this->state;

        return $object;
    }
    
}