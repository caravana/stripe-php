<?php

namespace jamesvweston\Stripe\Models\Requests;


use jamesvweston\Stripe\Models\Requests\Base\BaseCreateTransferRequest;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateTransferRequest extends BaseCreateTransferRequest
{

    /**
     * Created constructor.
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->amount                   = AU::get($data['amount']);
            $this->application_fee          = AU::get($data['application_fee']);
            $this->currency                 = AU::get($data['currency']);
            $this->destination              = AU::get($data['destination']);
            $this->description              = AU::get($data['description']);
            $this->metadata                 = AU::get($data['metadata']);
            $this->source_transaction       = AU::get($data['source_transaction']);
            $this->statement_descriptor     = AU::get($data['statement_descriptor']);
            $this->source_type              = AU::get($data['source_type']);
            $this->method                   = AU::get($data['method']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['amount']                   = $this->amount;
        $object['application_fee']          = $this->application_fee;
        $object['currency']                 = $this->currency;
        $object['destination']              = $this->destination;
        $object['description']              = $this->description;
        $object['metadata']                 = $this->metadata;
        $object['source_transaction']       = $this->source_transaction;
        $object['statement_descriptor']     = $this->statement_descriptor;
        $object['source_type']              = $this->source_type;
        $object['method']                   = $this->method;

        return $object;
    }
    
}