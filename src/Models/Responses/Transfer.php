<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseTransfer;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Transfer extends BaseTransfer
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->object                   = AU::get($data['object']);
            $this->amount                   = AU::get($data['amount']);
            $this->amount_reversed          = AU::get($data['amount_reversed']);
            $this->application_fee          = AU::get($data['application_fee']);
            $this->balance_transaction      = AU::get($data['balance_transaction']);
            $this->created                  = AU::get($data['created']);
            $this->currency                 = AU::get($data['currency']);
            $this->date                     = AU::get($data['date']);
            $this->description              = AU::get($data['description']);
            $this->destination              = AU::get($data['destination']);
            $this->destination_payment      = AU::get($data['destination_payment']);
            $this->failure_code             = AU::get($data['failure_code']);
            $this->failure_message          = AU::get($data['failure_message']);
            $this->livemode                 = AU::get($data['livemode']);
            $this->metadata                 = AU::get($data['metadata']);
            $this->method                   = AU::get($data['method']);
            $this->reversals                = AU::get($data['reversals']);
            $this->reversed                 = AU::get($data['reversed']);
            $this->source_transaction       = AU::get($data['source_transaction']);
            $this->source_type              = AU::get($data['source_type']);
            $this->statement_descriptor     = AU::get($data['statement_descriptor']);
            $this->status                   = AU::get($data['status']);
            $this->type                     = AU::get($data['type']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['object']                   = $this->object;
        $object['amount']                   = $this->amount;
        $object['amount_reversed']          = $this->amount_reversed;
        $object['application_fee']          = $this->application_fee;
        $object['balance_transaction']      = $this->balance_transaction;
        $object['created']                  = $this->created;
        $object['currency']                 = $this->currency;
        $object['date']                     = $this->date;
        $object['description']              = $this->description;
        $object['destination']              = $this->destination;
        $object['destination_payment']      = $this->destination_payment;
        $object['failure_code']             = $this->failure_code;
        $object['failure_message']          = $this->failure_message;
        $object['livemode']                 = $this->livemode;
        $object['metadata']                 = $this->metadata;
        $object['method']                   = $this->method;
        $object['reversals']                = $this->reversals;
        $object['reversed']                 = $this->reversed;
        $object['source_transaction']       = $this->source_transaction;
        $object['source_type']              = $this->source_type;
        $object['statement_descriptor']     = $this->statement_descriptor;
        $object['status']                   = $this->status;
        $object['type']                     = $this->type;

        return $object;
    }
    
}