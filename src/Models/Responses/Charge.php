<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseCharge;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Charge extends BaseCharge
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->object                   = AU::get($data['object']);
            $this->amount                   = AU::get($data['amount']);
            $this->amount_refunded          = AU::get($data['amount_refunded']);
            $this->application_fee          = AU::get($data['application_fee']);
            $this->balance_transaction      = AU::get($data['balance_transaction']);
            $this->captured                 = AU::get($data['captured']);
            $this->created                  = AU::get($data['created']);
            $this->currency                 = AU::get($data['currency']);
            $this->customer                 = AU::get($data['customer']);
            $this->description              = AU::get($data['description']);
            $this->destination              = AU::get($data['destination']);
            $this->dispute                  = !is_null(AU::get($data['dispute'])) ? new Dispute(AU::get($data['dispute'])) : null;
            $this->failure_code             = AU::get($data['failure_code']);
            $this->failure_message          = AU::get($data['failure_message']);
            $this->fraud_details            = AU::get($data['fraud_details']);
            $this->invoice                  = AU::get($data['invoice']);
            $this->livemode                 = AU::get($data['livemode']);
            $this->metadata                 = AU::get($data['metadata']);
            $this->order                    = AU::get($data['order']);
            $this->paid                     = AU::get($data['paid']);
            $this->receipt_email            = AU::get($data['receipt_email']);
            $this->receipt_number           = AU::get($data['receipt_number']);
            $this->refunded                 = AU::get($data['refunded']);
            $this->refunds                  = AU::get($data['refunds']);
            $this->shipping                 = AU::get($data['shipping']);
            $this->source                   = !is_null(AU::get($data['source'])) ? new Card(AU::get($data['source'])) : null;
            $this->source_transfer          = AU::get($data['source_transfer']);
            $this->statement_descriptor     = AU::get($data['statement_descriptor']);
            $this->status                   = AU::get($data['status']);
            $this->transfer                 = AU::get($data['transfer']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['object']                   = $this->object;
        $object['amount']                   = $this->amount;
        $object['amount_refunded']          = $this->amount_refunded;
        $object['application_fee']          = $this->application_fee;
        $object['balance_transaction']      = $this->balance_transaction;
        $object['captured']                 = $this->captured;
        $object['created']                  = $this->created;
        $object['currency']                 = $this->currency;
        $object['customer']                 = $this->customer;
        $object['description']              = $this->description;
        $object['destination']              = $this->destination;
        $object['dispute']                  = ($this->dispute instanceof \JsonSerializable) ? $this->dispute->jsonSerialize() : null;
        $object['failure_code']             = $this->failure_code;
        $object['failure_message']          = $this->failure_message;
        $object['fraud_details']            = $this->fraud_details;
        $object['invoice']                  = $this->invoice;
        $object['livemode']                 = $this->livemode;
        $object['metadata']                 = $this->metadata;
        $object['order']                    = $this->order;
        $object['paid']                     = $this->paid;
        $object['receipt_email']            = $this->receipt_email;
        $object['receipt_number']           = $this->receipt_number;
        $object['refunded']                 = $this->refunded;
        $object['refunds']                  = $this->refunds;
        $object['shipping']                 = $this->shipping;
        $object['source']                   = ($this->source instanceof \JsonSerializable) ? $this->source->jsonSerialize() : null;
        $object['source_transfer']          = $this->source_transfer;
        $object['statement_descriptor']     = $this->statement_descriptor;
        $object['status']                   = $this->status;
        $object['transfer']                 = $this->transfer;

        return $object;
    }
    
}