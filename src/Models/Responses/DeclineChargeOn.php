<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseDeclineChargeOn;
use jamesvweston\Utilities\ArrayUtil AS AU;

class DeclineChargeOn extends BaseDeclineChargeOn
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->avs_failure              = AU::get($data['avs_failure']);
            $this->cvc_failure              = AU::get($data['cvc_failure']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['avs_failure']              = $this->avs_failure;
        $object['cvc_failure']              = $this->cvc_failure;

        return $object;
    }
    
}