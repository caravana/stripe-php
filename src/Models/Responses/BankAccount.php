<?php

namespace jamesvweston\Stripe\Models\Responses;
use jamesvweston\Stripe\Models\Responses\Base\BaseBankAccount;
use jamesvweston\Utilities\ArrayUtil AS AU;

class BankAccount extends BaseBankAccount
{

    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->object                   = AU::get($data['object']);
            $this->account                  = AU::get($data['account']);
            $this->customer                 = AU::get($data['customer']);
            $this->account_holder_name      = AU::get($data['account_holder_name']);
            $this->account_holder_type      = AU::get($data['account_holder_type']);
            $this->bank_name                = AU::get($data['bank_name']);
            $this->country                  = AU::get($data['country']);
            $this->currency                 = AU::get($data['currency']);
            $this->default_for_currency     = AU::get($data['default_for_currency']);
            $this->fingerprint              = AU::get($data['fingerprint']);
            $this->last4                    = AU::get($data['last4']);
            $this->metadata                 = AU::get($data['metadata']);
            $this->routing_number           = AU::get($data['routing_number']);
            $this->status                   = AU::get($data['status']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['object']                   = $this->object;
        $object['account']                  = $this->account;
        $object['customer']                 = $this->customer;
        $object['account_holder_name']      = $this->account_holder_name;
        $object['account_holder_type']      = $this->account_holder_type;
        $object['bank_name']                = $this->bank_name;
        $object['country']                  = $this->country;
        $object['currency']                 = $this->currency;
        $object['default_for_currency']     = $this->default_for_currency;
        $object['fingerprint']              = $this->fingerprint;
        $object['last4']                    = $this->last4;
        $object['metadata']                 = $this->metadata;
        $object['routing_number']           = $this->routing_number;
        $object['status']                   = $this->status;

        return $object;
    }
    
}