<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseLegalEntityVerification;
use jamesvweston\Utilities\ArrayUtil AS AU;

class LegalEntityVerification extends BaseLegalEntityVerification
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->details              = AU::get($data['details']);
            $this->details_code         = AU::get($data['details_code']);
            $this->document             = AU::get($data['document']);
            $this->status               = AU::get($data['status']);
            
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['details']              = $this->details;
        $object['details_code']         = $this->details_code;
        $object['document']             = $this->document;
        $object['status']               = $this->status;

        return $object;
    }
    
}