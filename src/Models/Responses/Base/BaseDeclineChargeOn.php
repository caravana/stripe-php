<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\DeclineChargeOn AS DeclineChargeOnContract;

abstract class BaseDeclineChargeOn implements DeclineChargeOnContract
{

    /**
     * Whether or not Stripe should automatically decline charges with an incorrect zip/postal code. This setting only applies if a card includes a zip code and the bank specifically marks it as failed.
     * @var bool
     */
    protected $avs_failure;

    /**
     * Whether or not Stripe should automatically decline charges with an incorrect CVC. This setting only applies if a card includes a CVC and the bank specifically marks it as failed.
     * @var bool
     */
    protected $cvc_failure;

    /**
     * @return boolean
     */
    public function getAvsFailure()
    {
        return $this->avs_failure;
    }

    /**
     * @param boolean $avs_failure
     */
    public function setAvsFailure($avs_failure)
    {
        $this->avs_failure = $avs_failure;
    }

    /**
     * @return boolean
     */
    public function getCvcFailure()
    {
        return $this->cvc_failure;
    }

    /**
     * @param boolean $cvc_failure
     */
    public function setCvcFailure($cvc_failure)
    {
        $this->cvc_failure = $cvc_failure;
    }
    
}