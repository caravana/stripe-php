<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\BankAccount AS BankAccountContract;

/**
 * Class BaseBankAccount
 * @see     https://stripe.com/docs/api#customer_bank_account_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseBankAccount implements BankAccountContract
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $object = 'bank_account';

    /**
     * The account id that owns this bank account
     * @var string|null
     */
    protected $account;
    
    /**
     * The customer id that owns this bank account
     * @var string|null
     */
    protected $customer;

    /**
     * The name of the person or business that owns the bank account.
     * @var string
     */
    protected $account_holder_name;

    /**
     * The type of entity that holds the account.
     * This can be either individual or company
     * @var string
     */
    protected $account_holder_type;

    /**
     * Name of the bank associated with the routing number
     * e.g. WELLS FARGO
     * @var string
     */
    protected $bank_name;

    /**
     * Two-letter ISO code representing the country the bank account is located in.
     * @var string
     */
    protected $country;

    /**
     * Three-letter ISO currency code representing the currency paid out to the bank account.
     * @var string
     */
    protected $currency;

    /**
     * Whether this bank account is the default bank account for its currency.
     * @var bool
     */
    protected $default_for_currency;

    /**
     * Uniquely identifies this particular bank account.
     * You can use this attribute to check whether two bank accounts are the same.
     * @var string
     */
    protected $fingerprint;

    /**
     * @var string
     */
    protected $last4;

    /**
     * A set of key/value pairs that you can attach to a bank account object.
     * It can be useful for storing additional information about the bank account in a structured format.
     * @var array
     */
    protected $metadata;

    /**
     * The routing transit number for the bank account.
     * @var string
     */
    protected $routing_number;

    /**
     * Possible values are new, validated, verified, verification_failed, or errored
     * @var string
     */
    protected $status;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return null|string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param null|string $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param string $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getAccountHolderName()
    {
        return $this->account_holder_name;
    }

    /**
     * @param string $account_holder_name
     */
    public function setAccountHolderName($account_holder_name)
    {
        $this->account_holder_name = $account_holder_name;
    }

    /**
     * @return string
     */
    public function getAccountHolderType()
    {
        return $this->account_holder_type;
    }

    /**
     * @param string $account_holder_type
     */
    public function setAccountHolderType($account_holder_type)
    {
        $this->account_holder_type = $account_holder_type;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * @param string $bank_name
     */
    public function setBankName($bank_name)
    {
        $this->bank_name = $bank_name;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return boolean
     */
    public function isDefaultForCurrency()
    {
        return $this->default_for_currency;
    }

    /**
     * @param boolean $default_for_currency
     */
    public function setDefaultForCurrency($default_for_currency)
    {
        $this->default_for_currency = $default_for_currency;
    }

    /**
     * @return string
     */
    public function getFingerprint()
    {
        return $this->fingerprint;
    }

    /**
     * @param string $fingerprint
     */
    public function setFingerprint($fingerprint)
    {
        $this->fingerprint = $fingerprint;
    }

    /**
     * @return string
     */
    public function getLast4()
    {
        return $this->last4;
    }

    /**
     * @param string $last4
     */
    public function setLast4($last4)
    {
        $this->last4 = $last4;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return string
     */
    public function getRoutingNumber()
    {
        return $this->routing_number;
    }

    /**
     * @param string $routing_number
     */
    public function setRoutingNumber($routing_number)
    {
        $this->routing_number = $routing_number;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}