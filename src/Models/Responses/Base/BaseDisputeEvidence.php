<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\DisputeEvidence AS DisputeEvidenceContract;

/**
 * Class BaseDisputeEvidence
 * @see     https://stripe.com/docs/api#dispute_evidence_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseDisputeEvidence implements DisputeEvidenceContract
{

}