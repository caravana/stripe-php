<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Invoice AS InvoiceContract;

/**
 * Class BaseInvoice
 * @see     https://stripe.com/docs/api#invoices
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseInvoice implements InvoiceContract
{

}