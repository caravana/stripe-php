<?php

namespace jamesvweston\Stripe\Models\Responses\Base;
use jamesvweston\Stripe\Models\Responses\Contracts\Charge AS ChargeContract;


/**
 * Class BaseCharge
 * @see     https://stripe.com/docs/api#charge_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseCharge implements ChargeContract
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $object = 'charge';

    /**
     * A positive integer in the smallest currency unit
     * e.g. $1.00 = 100
     * The minimum amount is $0.50 US
     * @var int
     */
    protected $amount;

    /**
     * Amount in cents refunded
     * Can be less than the amount attribute on the charge if a partial refund was issued
     * @var int
     */
    protected $amount_refunded;

    /**
     * @var string|null
     */
    protected $application_fee;

    /**
     * ID of the balance transaction that describes the impact of this charge on your account balance
     * @var string
     */
    protected $balance_transaction;

    /**
     * If the charge was created without capturing, this boolean represents whether or not it is still uncaptured or has since been captured
     * @var bool|null
     */
    protected $captured;

    /**
     * timestamp
     * @var int
     */
    protected $created;

    /**
     * 3-letter ISO code for currency
     * @var string
     */
    protected $currency;

    /**
     * @var string|null
     */
    protected $customer;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * The account (if any) the charge was made on behalf of
     * @var string|null
     */
    protected $destination;

    /**
     * Details about the dispute if the charge has been disputed
     * @var BaseDispute|null
     */
    protected $dispute;

    /**
     * Error code explaining reason for charge failure if available
     * @var string|null
     */
    protected $failure_code;

    /**
     * Message to user further explaining reason for charge failure if available
     * @var string|null
     */
    protected $failure_message;

    /**
     * Hash with information on fraud assessments for the charge
     * @var array|null
     */
    protected $fraud_details;

    /**
     * ID of the invoice this charge is for if one exists
     * @var string|null
     */
    protected $invoice;

    /**
     * @var bool
     */
    protected $livemode;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * ID of the order this charge is for if one exists
     * @var string|null
     */
    protected $order;

    /**
     * true if the charge succeeded, or was successfully authorized for later capture
     * @var bool
     */
    protected $paid;

    /**
     * This is the email address that the receipt for this charge was sent to
     * @var string|null
     */
    protected $receipt_email;

    /**
     * This is the transaction number that appears on email receipts sent for this charge
     * @var string|null
     */
    protected $receipt_number;

    /**
     * Whether or not the charge has been fully refunded
     * f the charge is only partially refunded, this attribute will still be false
     * @var bool
     */
    protected $refunded;

    /**
     * @var array
     */
    protected $refunds;

    /**
     * Shipping information for the charge
     * @var array|null
     */
    protected $shipping;

    /**
     * @var BaseCard
     */
    protected $source;

    /**
     * The transfer ID which created this charge
     * Only present if the charge came from another Stripe account
     * @var string|null
     */
    protected $source_transfer;

    /**
     * Extra information about a charge
     * This will appear on your customer’s credit card statement
     * @var string|null
     */
    protected $statement_descriptor;

    /**
     * The status of the payment is either succeeded, pending, or failed
     * @var string
     */
    protected $status;

    /**
     * Only applicable if the charge was created using the destination parameter
     * ID of the transfer to the destination account
     * @var string|null
     */
    protected $transfer;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getAmountRefunded()
    {
        return $this->amount_refunded;
    }

    /**
     * @param int $amount_refunded
     */
    public function setAmountRefunded($amount_refunded)
    {
        $this->amount_refunded = $amount_refunded;
    }

    /**
     * @return null|string
     */
    public function getApplicationFee()
    {
        return $this->application_fee;
    }

    /**
     * @param null|string $application_fee
     */
    public function setApplicationFee($application_fee)
    {
        $this->application_fee = $application_fee;
    }

    /**
     * @return string
     */
    public function getBalanceTransaction()
    {
        return $this->balance_transaction;
    }

    /**
     * @param string $balance_transaction
     */
    public function setBalanceTransaction($balance_transaction)
    {
        $this->balance_transaction = $balance_transaction;
    }

    /**
     * @return bool|null
     */
    public function getCaptured()
    {
        return $this->captured;
    }

    /**
     * @param bool|null $captured
     */
    public function setCaptured($captured)
    {
        $this->captured = $captured;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return null|string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param null|string $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param null|string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return BaseDispute|null
     */
    public function getDispute()
    {
        return $this->dispute;
    }

    /**
     * @param BaseDispute|null $dispute
     */
    public function setDispute($dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * @return null|string
     */
    public function getFailureCode()
    {
        return $this->failure_code;
    }

    /**
     * @param null|string $failure_code
     */
    public function setFailureCode($failure_code)
    {
        $this->failure_code = $failure_code;
    }

    /**
     * @return null|string
     */
    public function getFailureMessage()
    {
        return $this->failure_message;
    }

    /**
     * @param null|string $failure_message
     */
    public function setFailureMessage($failure_message)
    {
        $this->failure_message = $failure_message;
    }

    /**
     * @return array|null
     */
    public function getFraudDetails()
    {
        return $this->fraud_details;
    }

    /**
     * @param array|null $fraud_details
     */
    public function setFraudDetails($fraud_details)
    {
        $this->fraud_details = $fraud_details;
    }

    /**
     * @return null|string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param null|string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return boolean
     */
    public function isLivemode()
    {
        return $this->livemode;
    }

    /**
     * @param boolean $livemode
     */
    public function setLivemode($livemode)
    {
        $this->livemode = $livemode;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return null|string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param null|string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return boolean
     */
    public function isPaid()
    {
        return $this->paid;
    }

    /**
     * @param boolean $paid
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    }

    /**
     * @return null|string
     */
    public function getReceiptEmail()
    {
        return $this->receipt_email;
    }

    /**
     * @param null|string $receipt_email
     */
    public function setReceiptEmail($receipt_email)
    {
        $this->receipt_email = $receipt_email;
    }

    /**
     * @return null|string
     */
    public function getReceiptNumber()
    {
        return $this->receipt_number;
    }

    /**
     * @param null|string $receipt_number
     */
    public function setReceiptNumber($receipt_number)
    {
        $this->receipt_number = $receipt_number;
    }

    /**
     * @return boolean
     */
    public function isRefunded()
    {
        return $this->refunded;
    }

    /**
     * @param boolean $refunded
     */
    public function setRefunded($refunded)
    {
        $this->refunded = $refunded;
    }

    /**
     * @return array
     */
    public function getRefunds()
    {
        return $this->refunds;
    }

    /**
     * @param array $refunds
     */
    public function setRefunds($refunds)
    {
        $this->refunds = $refunds;
    }

    /**
     * @return array|null
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param array|null $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return BaseCard
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param BaseCard $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return null|string
     */
    public function getSourceTransfer()
    {
        return $this->source_transfer;
    }

    /**
     * @param null|string $source_transfer
     */
    public function setSourceTransfer($source_transfer)
    {
        $this->source_transfer = $source_transfer;
    }

    /**
     * @return null|string
     */
    public function getStatementDescriptor()
    {
        return $this->statement_descriptor;
    }

    /**
     * @param null|string $statement_descriptor
     */
    public function setStatementDescriptor($statement_descriptor)
    {
        $this->statement_descriptor = $statement_descriptor;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return null|string
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * @param null|string $transfer
     */
    public function setTransfer($transfer)
    {
        $this->transfer = $transfer;
    }

}