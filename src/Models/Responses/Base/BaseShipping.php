<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Shipping AS ShippingContract;

abstract class BaseShipping implements ShippingContract
{

    /**
     * @var BaseAddress
     */
    protected $address;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $phone;

    /**
     * @return BaseAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param BaseAddress $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
}