<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Transfer AS TransferContract;


/**
 * Class BaseTransfer
 * @see     https://stripe.com/docs/api#transfer_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseTransfer implements TransferContract
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $object = 'transfer';

    /**
     * Amount (in cents) to be transferred
     * @var int
     */
    protected $amount;

    /**
     * Amount in cents reversed
     * can be less than the amount attribute on the transfer if a partial reversal was issued
     * @var int
     */
    protected $amount_reversed;

    /**
     * @var string
     */
    protected $application_fee;

    /**
     * Balance transaction that describes the impact of this transfer on your account balance
     * @var string
     */
    protected $balance_transaction;

    /**
     * timestamp
     * @var int
     */
    protected $created;

    /**
     * @var string
     */
    protected $currency;

    /**
     * Date the transfer is scheduled to arrive in the bank
     * This factors in delays like weekends or bank holidays
     * @var int
     */
    protected $date;

    /**
     * Internal-only description of the transfer
     * @var string|null
     */
    protected $description;

    /**
     * ID of the bank account, card, or Stripe account the transfer was sent to
     * @var string
     */
    protected $destination;

    /**
     * If the destination is a Stripe account, this will be the ID of the payment that the destination account received for the transfer
     * @var string|null
     */
    protected $destination_payment;

    /**
     * Error code explaining reason for transfer failure if available
     * @var string|null
     */
    protected $failure_code;


    /**
     * Message to user further explaining reason for transfer failure if available
     * @var string|null
     */
    protected $failure_message;

    /**
     * @var bool
     */
    protected $livemode;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * The method used to send this transfer, which can be standard or instant.
     * instant is only supported for transfers to debit cards.
     * @var string
     */
    protected $method;

    /**
     * A list of reversals that have been applied to the transfer
     * @var array
     */
    protected $reversals;

    /**
     * Whether or not the transfer has been fully reversed
     * If the transfer is only partially reversed, this attribute will still be false
     * @var bool
     */
    protected $reversed;

    /**
     * ID of the charge (or other transaction) that was used to fund the transfer
     * If null, the transfer was funded from the available balance
     * @var string|null
     */
    protected $source_transaction;

    /**
     * The source balance this transfer came from. One of card, bank_account, bitcoin_receiver, or alipay_account
     * @var string
     */
    protected $source_type;

    /**
     * Extra information about a transfer to be displayed on the user’s bank statement
     * @var string|null
     */
    protected $statement_descriptor;

    /**
     * Current status of the transfer
     * paid, pending, in_transit, canceled or failed
     * A transfer will be pending until it is submitted to the bank, at which point it becomes in_transit. It will then change to paid if the transaction goes through
     * If it does not go through successfully, its status will change to failed or canceled
     * @var string
     */
    protected $status;

    /**
     * Can be card, bank_account, or stripe_account
     * @var string
     */
    protected $type;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getAmountReversed()
    {
        return $this->amount_reversed;
    }

    /**
     * @param int $amount_reversed
     */
    public function setAmountReversed($amount_reversed)
    {
        $this->amount_reversed = $amount_reversed;
    }

    /**
     * @return string
     */
    public function getApplicationFee()
    {
        return $this->application_fee;
    }

    /**
     * @param string $application_fee
     */
    public function setApplicationFee($application_fee)
    {
        $this->application_fee = $application_fee;
    }

    /**
     * @return string
     */
    public function getBalanceTransaction()
    {
        return $this->balance_transaction;
    }

    /**
     * @param string $balance_transaction
     */
    public function setBalanceTransaction($balance_transaction)
    {
        $this->balance_transaction = $balance_transaction;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return null|string
     */
    public function getDestinationPayment()
    {
        return $this->destination_payment;
    }

    /**
     * @param null|string $destination_payment
     */
    public function setDestinationPayment($destination_payment)
    {
        $this->destination_payment = $destination_payment;
    }

    /**
     * @return null|string
     */
    public function getFailureCode()
    {
        return $this->failure_code;
    }

    /**
     * @param null|string $failure_code
     */
    public function setFailureCode($failure_code)
    {
        $this->failure_code = $failure_code;
    }

    /**
     * @return null|string
     */
    public function getFailureMessage()
    {
        return $this->failure_message;
    }

    /**
     * @param null|string $failure_message
     */
    public function setFailureMessage($failure_message)
    {
        $this->failure_message = $failure_message;
    }

    /**
     * @return boolean
     */
    public function isLivemode()
    {
        return $this->livemode;
    }

    /**
     * @param boolean $livemode
     */
    public function setLivemode($livemode)
    {
        $this->livemode = $livemode;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getReversals()
    {
        return $this->reversals;
    }

    /**
     * @param array $reversals
     */
    public function setReversals($reversals)
    {
        $this->reversals = $reversals;
    }

    /**
     * @return boolean
     */
    public function isReversed()
    {
        return $this->reversed;
    }

    /**
     * @param boolean $reversed
     */
    public function setReversed($reversed)
    {
        $this->reversed = $reversed;
    }

    /**
     * @return null|string
     */
    public function getSourceTransaction()
    {
        return $this->source_transaction;
    }

    /**
     * @param null|string $source_transaction
     */
    public function setSourceTransaction($source_transaction)
    {
        $this->source_transaction = $source_transaction;
    }

    /**
     * @return string
     */
    public function getSourceType()
    {
        return $this->source_type;
    }

    /**
     * @param string $source_type
     */
    public function setSourceType($source_type)
    {
        $this->source_type = $source_type;
    }

    /**
     * @return null|string
     */
    public function getStatementDescriptor()
    {
        return $this->statement_descriptor;
    }

    /**
     * @param null|string $statement_descriptor
     */
    public function setStatementDescriptor($statement_descriptor)
    {
        $this->statement_descriptor = $statement_descriptor;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
    
}