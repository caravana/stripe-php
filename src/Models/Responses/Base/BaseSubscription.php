<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Subscription AS SubscriptionContract;

/**
 * Class BaseSubscription
 * @see https://stripe.com/docs/api#subscriptions
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseSubscription implements SubscriptionContract
{

}