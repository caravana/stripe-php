<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\TransferSchedule AS TransferScheduleContract;

abstract class BaseTransferSchedule implements TransferScheduleContract
{

    /**
     * When payments collected will be automatically paid out to the account holder’s bank account
     * @var int
     */
    protected $delay_days;

    /**
     * How frequently funds will be paid out
     * One of manual (transfers only created via API call), daily, weekly, or monthly
     * @var string
     */
    protected $interval;

    /**
     * The day of the month funds will be paid out. Only shown if interval is monthly
     * @var int
     */
    protected $monthly_anchor;

    /**
     * The day of the week funds will be paid out, of the style ‘monday’, ‘tuesday’, etc. 
     * Only shown if interval is weekly
     * @var string|null
     */
    protected $weekly_anchor;

    /**
     * @return int
     */
    public function getDelayDays()
    {
        return $this->delay_days;
    }

    /**
     * @param int $delay_days
     */
    public function setDelayDays($delay_days)
    {
        $this->delay_days = $delay_days;
    }

    /**
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @param string $interval
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;
    }

    /**
     * @return int
     */
    public function getMonthlyAnchor()
    {
        return $this->monthly_anchor;
    }

    /**
     * @param int $monthly_anchor
     */
    public function setMonthlyAnchor($monthly_anchor)
    {
        $this->monthly_anchor = $monthly_anchor;
    }

    /**
     * @return null|string
     */
    public function getWeeklyAnchor()
    {
        return $this->weekly_anchor;
    }

    /**
     * @param null|string $weekly_anchor
     */
    public function setWeeklyAnchor($weekly_anchor)
    {
        $this->weekly_anchor = $weekly_anchor;
    }
    
}