<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\TransferReversal AS TransferReversalContract;

/**
 * Class BaseTransferReversal
 * @see     https://stripe.com/docs/api#transfer_reversal_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseTransferReversal implements TransferReversalContract
{

}