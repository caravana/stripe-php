<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Discount AS DiscountContract;

/**
 * Class BaseDiscount
 * @see     https://stripe.com/docs/api#discount_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseDiscount implements DiscountContract
{

    /**
     * @var string
     */
    protected $object       = 'discount';

    /**
     * @var BaseCoupon
     */
    protected $coupon;

    /**
     * Customer id
     * @var string
     */
    protected $customer;


    /**
     * If the coupon has a duration of once or repeating, the date that this discount will end.
     * If the coupon used has a forever duration, this attribute will be null.
     * @var int|null
     */
    protected $end;

    /**
     * Date that the coupon was applied.
     * @var int
     */
    protected $start;

    /**
     * The subscription that this coupon is applied to, if it is applied to a particular subscription.
     * @var string|null
     */
    protected $subscription;

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @param Coupon $coupon
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param string $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return int|null
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param int|null $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return int
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param int $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return null|string
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param null|string $subscription
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
    }
    
}