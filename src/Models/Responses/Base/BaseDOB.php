<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\DOB AS DOBContract;

abstract class BaseDOB implements DOBContract
{

    /**
     * The day of birth, between 1 and 31
     * @var int
     */
    protected $day;

    /**
     * The month of birth, between 1 and 12
     * @var int
     */
    protected $month;

    /**
     * The 4-digit year of birth
     * @var int
     */
    protected $year;

    /**
     * @return int
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param int $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param int $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }
    
}