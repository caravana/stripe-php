<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Balance AS BalanceContract;

/**
 * Class BaseBalance
 * @see     https://stripe.com/docs/api#balance_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseBalance implements BalanceContract
{

}