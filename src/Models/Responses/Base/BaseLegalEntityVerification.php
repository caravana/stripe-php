<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\LegalEntityVerification AS LegalEntityVerificationContract;

abstract class BaseLegalEntityVerification implements LegalEntityVerificationContract
{

    /**
     * A user-displayable string describing the verification state for this legal entity
     * For example, if a document is uploaded and the picture is too fuzzy
     * @var string
     */
    protected $details;

    /**
     * A machine-readable code specifying the verification state for this legal entity
     * @var string
     */
    protected $details_code;

    /**
     * ID of a file upload
     * A photo (jpg or png) of an identifying document, either a passport or local ID card
     * @var string|null
     */
    protected $document;

    /**
     * The state of verification for this legal entity
     * Possible values are unverified, pending, or verified
     * @var string
     */
    protected $status;

    /**
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param string $details
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }

    /**
     * @return string
     */
    public function getDetailsCode()
    {
        return $this->details_code;
    }

    /**
     * @param string $details_code
     */
    public function setDetailsCode($details_code)
    {
        $this->details_code = $details_code;
    }

    /**
     * @return null|string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param null|string $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
}