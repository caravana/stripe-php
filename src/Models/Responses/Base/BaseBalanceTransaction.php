<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Collections\TransferCollection;
use jamesvweston\Stripe\Models\Responses\Contracts\BaseBalanceTransaction AS BaseBalanceTransactionContract;
/**
 * Class BaseBalanceTransaction
 * @see     https://stripe.com/docs/api#balance_transaction_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseBalanceTransaction implements BaseBalanceTransactionContract
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $object;

    /**
     * Gross amount of the transaction, in cents
     * @var int
     */
    protected $amount;

    /**
     * The date the transaction’s net funds will become available in the Stripe balance
     * @var int
     */
    protected $available_on;

    /**
     * @var int
     */
    protected $created;

    /**
     * @var string
     */
    protected $currency;

    /**
     * @var string
     */
    protected $description;

    /**
     * Fees (in cents) paid for this transaction
     * @var int
     */
    protected $fee;

    /**
     * @var BaseFeeDetail[]
     */
    protected $fee_details;

    /**
     * Net amount of the transaction, in cents
     * @var int
     */
    protected $net;

    /**
     * The Stripe object this transaction is related to
     * @var string
     */
    protected $source;

    /**
     * @var TransferCollection
     */
    protected $sourced_transfers;

    /**
     * If the transaction’s net funds are available in the Stripe balance yet
     * One of: available, pending
     * @var string
     */
    protected $status;

    /**
     * Transaction type
     * One of: adjustment, application_fee, application_fee_refund, charge, payment, payment_refund, refund, transfer, transfer_cancel, transfer_failure, transfer_refund
     * @var string
     */
    protected $type;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getAvailableOn()
    {
        return $this->available_on;
    }

    /**
     * @param int $available_on
     */
    public function setAvailableOn($available_on)
    {
        $this->available_on = $available_on;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @param int $fee
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
    }

    /**
     * @return BaseFeeDetail[]
     */
    public function getFeeDetails()
    {
        return $this->fee_details;
    }

    /**
     * @param BaseFeeDetail[] $fee_details
     */
    public function setFeeDetails($fee_details)
    {
        $this->fee_details = $fee_details;
    }

    /**
     * @return int
     */
    public function getNet()
    {
        return $this->net;
    }

    /**
     * @param int $net
     */
    public function setNet($net)
    {
        $this->net = $net;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return TransferCollection
     */
    public function getSourcedTransfers()
    {
        return $this->sourced_transfers;
    }

    /**
     * @param TransferCollection $sourced_transfers
     */
    public function setSourcedTransfers($sourced_transfers)
    {
        $this->sourced_transfers = $sourced_transfers;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}