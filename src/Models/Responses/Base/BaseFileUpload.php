<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\FileUpload AS FileUploadContract;

/**
 * Class BaseFileUpload
 * @see     https://stripe.com/docs/api#file_upload_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseFileUpload implements FileUploadContract
{

}