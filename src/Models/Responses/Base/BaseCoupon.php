<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Coupon AS CouponContract;

/**
 * Class BaseCoupon
 * @see     https://stripe.com/docs/api#coupon_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseCoupon implements CouponContract
{

    /**
     * @var string
     */
    protected $id;


    /**
     * Amount that will be taken off the subtotal of any invoices for this customer
     * Required if currency is provided
     * @var int|null
     */
    protected $amount_off;

    /**
     * timestamp
     * @var int
     */
    protected $created;

    /**
     * The currency of the amount to take off.
     * Required if amount_off is provided
     * @var string|null
     */
    protected $currency;

    /**
     * Possible values: forever, once, repeating
     * @var string
     */
    protected $duration;

    /**
     * If duration is repeating this is required
     * null if duration is forever or once
     * @var int|null
     */
    protected $duration_in_months;

    /**
     * @var bool
     */
    protected $livemode;

    /**
     * Maximum number of times this coupon can be redeemed, in total, before it is no longer valid
     * @var int|null
     */
    protected $max_redemptions;

    /**
     * A set of key/value pairs that you can attach to a coupon object
     * @var array
     */
    protected $metadata;

    /**
     * Percent that will be taken off the subtotal of any invoices for this customer for the duration of the coupon
     * For example, a coupon with percent_off of 50 will make a $100 invoice $50 instead.
     * @var int
     */
    protected $percent_off;

    /**
     * Date after which the coupon can no longer be redeemed
     * @var int|null
     */
    protected $redeem_by;

    /**
     * Number of times this coupon has been applied to a customer
     * @var int
     */
    protected $times_redeemed;

    /**
     * Taking account of the above properties, whether this coupon can still be applied to a customer
     * @var bool
     */
    protected $valid;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getAmountOff()
    {
        return $this->amount_off;
    }

    /**
     * @param int|null $amount_off
     */
    public function setAmountOff($amount_off)
    {
        $this->amount_off = $amount_off;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return null|string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param null|string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return int|null
     */
    public function getDurationInMonths()
    {
        return $this->duration_in_months;
    }

    /**
     * @param int|null $duration_in_months
     */
    public function setDurationInMonths($duration_in_months)
    {
        $this->duration_in_months = $duration_in_months;
    }

    /**
     * @return boolean
     */
    public function isLivemode()
    {
        return $this->livemode;
    }

    /**
     * @param boolean $livemode
     */
    public function setLivemode($livemode)
    {
        $this->livemode = $livemode;
    }

    /**
     * @return int|null
     */
    public function getMaxRedemptions()
    {
        return $this->max_redemptions;
    }

    /**
     * @param int|null $max_redemptions
     */
    public function setMaxRedemptions($max_redemptions)
    {
        $this->max_redemptions = $max_redemptions;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return int
     */
    public function getPercentOff()
    {
        return $this->percent_off;
    }

    /**
     * @param int $percent_off
     */
    public function setPercentOff($percent_off)
    {
        $this->percent_off = $percent_off;
    }

    /**
     * @return int|null
     */
    public function getRedeemBy()
    {
        return $this->redeem_by;
    }

    /**
     * @param int|null $redeem_by
     */
    public function setRedeemBy($redeem_by)
    {
        $this->redeem_by = $redeem_by;
    }

    /**
     * @return int
     */
    public function getTimesRedeemed()
    {
        return $this->times_redeemed;
    }

    /**
     * @param int $times_redeemed
     */
    public function setTimesRedeemed($times_redeemed)
    {
        $this->times_redeemed = $times_redeemed;
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        return $this->valid;
    }

    /**
     * @param boolean $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    }
    
}