<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\LegalEntity AS LegalEntityContract;

abstract class BaseLegalEntity implements LegalEntityContract
{

    /**
     * @var array
     */
    protected $additional_owners;

    /**
     * The primary address of the legal entity
     * @var BaseAddress
     */
    protected $address;

    /**
     * The legal name of the company
     * @var string
     */
    protected $business_name;

    /**
     * If the business ID number of the legal entity has been provided
     * @var bool
     */
    protected $business_tax_id_provided;

    /**
     * @var BaseDOB
     */
    protected $dob;

    /**
     * The first name of the individual responsible for the account
     * @var string
     */
    protected $first_name;

    /**
     * The last name of the individual responsible for the account
     * @var string
     */
    protected $last_name;

    /**
     * The primary address of the individual responsible for the account
     * @var BaseAddress
     */
    protected $personal_address;

    /**
     * Whether the personal id number of the individual responsible for the account has been provided
     * @var bool
     */
    protected $personal_id_number_provided;

    /**
     * The phone number of the company, used for verification
     * @var string|null
     */
    protected $phone_number;

    /**
     * Whether the last 4 ssn digits of the individual responsible for the account have been provided
     * @var bool
     */
    protected $ssn_last_4_provided;

    /**
     * Either “individual” or “company”, for what kind of legal entity the account owner is for
     * @var string
     */
    protected $type;

    /**
     * The verification status of this business and responsible individual
     * @var BaseLegalEntityVerification
     */
    protected $verification;

    /**
     * @return array
     */
    public function getAdditionalOwners()
    {
        return $this->additional_owners;
    }

    /**
     * @param array $additional_owners
     */
    public function setAdditionalOwners($additional_owners)
    {
        $this->additional_owners = $additional_owners;
    }

    /**
     * @return BaseAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param BaseAddress $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getBusinessName()
    {
        return $this->business_name;
    }

    /**
     * @param string $business_name
     */
    public function setBusinessName($business_name)
    {
        $this->business_name = $business_name;
    }

    /**
     * @return boolean
     */
    public function isBusinessTaxIdProvided()
    {
        return $this->business_tax_id_provided;
    }

    /**
     * @param boolean $business_tax_id_provided
     */
    public function setBusinessTaxIdProvided($business_tax_id_provided)
    {
        $this->business_tax_id_provided = $business_tax_id_provided;
    }

    /**
     * @return BaseDOB
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param BaseDOB $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return BaseAddress
     */
    public function getPersonalAddress()
    {
        return $this->personal_address;
    }

    /**
     * @param BaseAddress $personal_address
     */
    public function setPersonalAddress($personal_address)
    {
        $this->personal_address = $personal_address;
    }

    /**
     * @return boolean
     */
    public function isPersonalIdNumberProvided()
    {
        return $this->personal_id_number_provided;
    }

    /**
     * @param boolean $personal_id_number_provided
     */
    public function setPersonalIdNumberProvided($personal_id_number_provided)
    {
        $this->personal_id_number_provided = $personal_id_number_provided;
    }

    /**
     * @return null|string
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param null|string $phone_number
     */
    public function setPhoneNumber($phone_number)
    {
        $this->phone_number = $phone_number;
    }

    /**
     * @return boolean
     */
    public function isSsnLast4Provided()
    {
        return $this->ssn_last_4_provided;
    }

    /**
     * @param boolean $ssn_last_4_provided
     */
    public function setSsnLast4Provided($ssn_last_4_provided)
    {
        $this->ssn_last_4_provided = $ssn_last_4_provided;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return BaseLegalEntityVerification
     */
    public function getVerification()
    {
        return $this->verification;
    }

    /**
     * @param BaseLegalEntityVerification $verification
     */
    public function setVerification($verification)
    {
        $this->verification = $verification;
    }
    
}