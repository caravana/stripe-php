<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Verification AS VerificationContract;

abstract class BaseVerification implements VerificationContract
{

    /**
     * A string describing the reason for this account being unable to charge and/or transfer
     * If that is the case. Possible values are:
     * rejected.fraud, rejected.terms_of_service, rejected.listed, rejected.other, fields_needed, listed, or other
     * @var string
     */
    protected $disabled_reason;

    /**
     * At what time the fields_needed must be provided
     * @var int
     */
    protected $due_by;

    /**
     * Field names that need to be provided for the account to remain in good standing
     * @var array
     */
    protected $fields_needed;

    /**
     * @return string
     */
    public function getDisabledReason()
    {
        return $this->disabled_reason;
    }

    /**
     * @param string $disabled_reason
     */
    public function setDisabledReason($disabled_reason)
    {
        $this->disabled_reason = $disabled_reason;
    }

    /**
     * @return int
     */
    public function getDueBy()
    {
        return $this->due_by;
    }

    /**
     * @param int $due_by
     */
    public function setDueBy($due_by)
    {
        $this->due_by = $due_by;
    }

    /**
     * @return array
     */
    public function getFieldsNeeded()
    {
        return $this->fields_needed;
    }

    /**
     * @param array $fields_needed
     */
    public function setFieldsNeeded($fields_needed)
    {
        $this->fields_needed = $fields_needed;
    }
    
}