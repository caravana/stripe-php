<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Dispute AS DisputeContract;

/**
 * Class BaseDispute
 * @see     https://stripe.com/docs/api#dispute_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseDispute implements DisputeContract
{

}