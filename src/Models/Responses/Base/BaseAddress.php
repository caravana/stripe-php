<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Address AS AddressContract;

abstract class BaseAddress implements AddressContract
{

    /**
     * City/Suburb/Town/Village
     * @var string
     */
    protected $city;

    /**
     * 2-letter country code
     * @var string
     */
    protected $country;

    /**
     * Address line 1 (Street address/PO Box/Company name)
     * @var string
     */
    protected $line1;

    /**
     * Address line 2 (Apartment/Suite/Unit/Building)
     * @var string|null
     */
    protected $line2;

    /**
     * Zip/Postal Code
     * @var string
     */
    protected $postal_code;

    /**
     * State/Province/County
     * @var string
     */
    protected $state;

    
    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param string $line1
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;
    }

    /**
     * @return null|string
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param null|string $line2
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param string $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }
    
}