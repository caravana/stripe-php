<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\TOSAcceptance AS TOSAcceptanceContract;

abstract class BaseTOSAcceptance implements TOSAcceptanceContract
{

    /**
     * The timestamp when the account owner accepted Stripe’s terms
     * @var int
     */
    protected $date;

    /**
     * The IP address from which the account owner accepted Stripe’s terms
     * @var string
     */
    protected $ip;

    /**
     * The user agent of the browser from which the user accepted Stripe’s terms
     * @var string
     */
    protected $user_agent;

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * @param string $user_agent
     */
    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;
    }
    
}