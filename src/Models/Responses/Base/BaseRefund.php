<?php

namespace jamesvweston\Stripe\Models\Responses\Base;
use jamesvweston\Stripe\Models\Responses\Contracts\Refund AS RefundContract;


/**
 * Class BaseRefund
 * @see     https://stripe.com/docs/api#refund_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseRefund implements RefundContract
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $object = 'refund';

    /**
     * Amount, in cents
     * @var int
     */
    protected $amount;

    /**
     * Balance transaction that describes the impact on your account balance
     * @var string
     */
    protected $balance_transaction;

    /**
     * ID of the charge that was refunded
     * @var string
     */
    protected $charge;

    /**
     * timestamp
     * @var int
     */
    protected $created;

    /**
     * Three-letter ISO code representing the currency
     * @var string
     */
    protected $currency;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * Reason for the refund
     * If set, possible values are duplicate, fraudulent, and requested_by_customer
     * @var string|null
     */
    protected $reason;

    /**
     * This is the transaction number that appears on email receipts sent for this refund
     * @var string|null
     */
    protected $receipt_number;

    /**
     * For credit card refunds, this will always be succeeded
     * For other types of refunds, it can be pending, succeeded, failed, or cancelled
     * @var string
     */
    protected $status;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getBalanceTransaction()
    {
        return $this->balance_transaction;
    }

    /**
     * @param string $balance_transaction
     */
    public function setBalanceTransaction($balance_transaction)
    {
        $this->balance_transaction = $balance_transaction;
    }

    /**
     * @return string
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * @param string $charge
     */
    public function setCharge($charge)
    {
        $this->charge = $charge;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return null|string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param null|string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return null|string
     */
    public function getReceiptNumber()
    {
        return $this->receipt_number;
    }

    /**
     * @param null|string $receipt_number
     */
    public function setReceiptNumber($receipt_number)
    {
        $this->receipt_number = $receipt_number;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}