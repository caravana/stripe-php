<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Event AS EventContract;

/**
 * Class BaseEvent
 * @see     https://stripe.com/docs/api#event_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseEvent implements EventContract
{

}