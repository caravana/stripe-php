<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Plan AS PlanContract;

/**
 * Class BasePlan
 * @see     https://stripe.com/docs/api#plan_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BasePlan implements PlanContract
{

    /**
     * @var string
     */
    protected $id;

    /**
     * The amount in cents to be charged on the interval specified
     * @var int
     */
    protected $amount;

    /**
     * @var int
     */
    protected $created;

    /**
     * @var string      Currency in which subscription will be charged
     */
    protected $currency;

    /**
     * One of day, week, month or year
     * The frequency with which a subscription should be billed.
     * @var string
     */
    protected $interval;

    /**
     * The number of intervals (specified in the interval property) between each subscription billing.
     * For example, interval=month and interval_count=3 bills every 3 months.
     * @var int
     */
    protected $interval_count;

    /**
     * @var bool
     */
    protected $livemode;

    /**
     * A set of key/value pairs that you can attach to a plan object
     * @var array
     */
    protected $metadata;

    /**
     * Display name of the plan
     * @var string
     */
    protected $name;

    /**
     * Extra information about a charge for the customer’s credit card statement
     * @var string|null
     */
    protected $statement_descriptor;

    /**
     * Number of trial period days granted when subscribing a customer to this plan.
     * Null if the plan has no trial period.
     * @var int|null
     */
    protected $trial_period_days;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @param string $interval
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;
    }

    /**
     * @return int
     */
    public function getIntervalCount()
    {
        return $this->interval_count;
    }

    /**
     * @param int $interval_count
     */
    public function setIntervalCount($interval_count)
    {
        $this->interval_count = $interval_count;
    }

    /**
     * @return boolean
     */
    public function isLivemode()
    {
        return $this->livemode;
    }

    /**
     * @param boolean $livemode
     */
    public function setLivemode($livemode)
    {
        $this->livemode = $livemode;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getStatementDescriptor()
    {
        return $this->statement_descriptor;
    }

    /**
     * @param null|string $statement_descriptor
     */
    public function setStatementDescriptor($statement_descriptor)
    {
        $this->statement_descriptor = $statement_descriptor;
    }

    /**
     * @return int|null
     */
    public function getTrialPeriodDays()
    {
        return $this->trial_period_days;
    }

    /**
     * @param int|null $trial_period_days
     */
    public function setTrialPeriodDays($trial_period_days)
    {
        $this->trial_period_days = $trial_period_days;
    }
    
}