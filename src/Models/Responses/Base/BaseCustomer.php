<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Customer AS CustomerContract;

/**
 * Class BaseCustomer
 * @see     https://stripe.com/docs/api#customer_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseCustomer implements CustomerContract
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $object       = 'customer';

    /**
     * @var float
     */
    protected $account_balance;

    /**
     * @var string|null
     */
    protected $business_vat_id;

    /**
     * @var int
     */
    protected $created;

    /**
     * @var string|null
     */
    protected $currency;

    /**
     * @var string|null
     */
    protected $default_source;

    /**
     * @var bool
     */
    protected $delinquent;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var BaseDiscount|null
     */
    protected $discount;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var bool
     */
    protected $liveMode;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * @var BaseShipping|null
     */
    protected $shipping;

    /**
     * @var BaseCollection|null
     */
    protected $sources;

    /**
     * @var BaseCollection|null
     */
    protected $subscriptions;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return float
     */
    public function getAccountBalance()
    {
        return $this->account_balance;
    }

    /**
     * @param float $account_balance
     */
    public function setAccountBalance($account_balance)
    {
        $this->account_balance = $account_balance;
    }

    /**
     * @return null|string
     */
    public function getBusinessVatId()
    {
        return $this->business_vat_id;
    }

    /**
     * @param null|string $business_vat_id
     */
    public function setBusinessVatId($business_vat_id)
    {
        $this->business_vat_id = $business_vat_id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return null|string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param null|string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return null|string
     */
    public function getDefaultSource()
    {
        return $this->default_source;
    }

    /**
     * @param null|string $default_source
     */
    public function setDefaultSource($default_source)
    {
        $this->default_source = $default_source;
    }

    /**
     * @return boolean
     */
    public function isDelinquent()
    {
        return $this->delinquent;
    }

    /**
     * @param boolean $delinquent
     */
    public function setDelinquent($delinquent)
    {
        $this->delinquent = $delinquent;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return BaseDiscount|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param BaseDiscount|null $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return boolean
     */
    public function isLiveMode()
    {
        return $this->liveMode;
    }

    /**
     * @param boolean $liveMode
     */
    public function setLiveMode($liveMode)
    {
        $this->liveMode = $liveMode;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return BaseShipping|null
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param BaseShipping|null $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return BaseCollection|null
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * @param BaseCollection|null $sources
     */
    public function setSources($sources)
    {
        $this->sources = $sources;
    }

    /**
     * @return BaseCollection|null
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @param BaseCollection|null $subscriptions
     */
    public function setSubscriptions($subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }
    
}