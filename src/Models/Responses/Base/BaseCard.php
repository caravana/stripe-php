<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Card AS CardContract;

/**
 * Class BaseCard
 * @see     https://stripe.com/docs/api#card_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseCard implements CardContract
{

    /**
     * ID of card (used in conjunction with a customer or recipient ID)
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $object           = 'card';

    /**
     * MANAGED ACCOUNTS ONLY
     * The account this card belongs to.
     * This attribute will not be in the card object if the card belongs to a customer or recipient instead.
     * @var BaseAccount|null
     */
    protected $account;

    /**
     * @var string|null
     */
    protected $address_city;

    /**
     * Billing address country, if provided when creating card
     * @var string|null
     */
    protected $address_country;

    /**
     * @var string|null
     */
    protected $address_line1;

    /**
     * If address_line1 was provided, results of the check.
     * One of: pass, fail, unavailable, unchecked
     * @var string|null
     */
    protected $address_line1_check;

    /**
     * @var string|null
     */
    protected $address_line2;

    /**
     * @var string|null
     */
    protected $address_state;

    /**
     * @var string|null
     */
    protected $address_zip;

    /**
     * If address_zip was provided, results of the check
     * One of: pass, fail, unavailable, unchecked
     * @var string|null
     */
    protected $address_zip_check;

    /**
     * Card brand
     * One of: Visa, American Express, MasterCard, Discover, JCB, Diners Club, Unknown
     * @var string
     */
    protected $brand;

    /**
     * Two-letter ISO code representing the country of the card
     * You could use this attribute to get a sense of the international breakdown of cards you’ve collected
     * @var string
     */
    protected $country;

    /**
     * MANAGED ACCOUNTS ONLY
     * Only applicable on accounts (not customers or recipients).
     * The card can be used as a transfer destination for funds in this currency
     * @var string|null
     */
    protected $currency;

    /**
     * The customer that this card belongs to.
     * This attribute will not be in the card object if the card belongs to an account or recipient instead
     * @var string
     */
    protected $customer;

    /**
     * If a CVC was provided, results of the check
     * One of: pass, fail, unavailable, unchecked
     * @var string|null
     */
    protected $cvc_check;

    /**
     * MANAGED ACCOUNTS ONLY
     * Only applicable on accounts (not customers or recipients).
     * This indicates whether or not this card is the default external account for its currency.
     * @var bool|null
     */
    protected $default_for_currency;

    /**
     * (For tokenized numbers only.) The last four digits of the device account number
     * @var string|null
     */
    protected $dynamic_last4;

    /**
     * @var int
     */
    protected $exp_month;

    /**
     * @var int
     */
    protected $exp_year;

    /**
     * Uniquely identifies this particular card number. 
     * You can use this attribute to check whether two customers who’ve signed up with you are using the same card number, for example
     * @var string|null
     */
    protected $fingerprint;

    /**
     * Card funding type.
     * Can be credit, debit, prepaid, or unknown
     * @var string
     */
    protected $funding;

    /**
     * @var string
     */
    protected $last4;

    /**
     * A set of key/value pairs that you can attach to a card object.
     * It can be useful for storing additional information about the card in a structured format.
     * @var array
     */
    protected $metadata;

    /**
     * Card holder name
     * @var string|null
     */
    protected $name;

    /**
     * The recipient that this card belongs to.
     * This attribute will not be in the card object if the card belongs to a customer or account instead.
     * @var string|null
     */
    protected $recipient;

    /**
     * If the card number is tokenized, this is the method that was used.
     * Can be apple_pay or android_pay
     * @var string|null
     */
    protected $tokenization_method;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return BaseAccount|null
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param BaseAccount|null $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return null|string
     */
    public function getAddressCity()
    {
        return $this->address_city;
    }

    /**
     * @param null|string $address_city
     */
    public function setAddressCity($address_city)
    {
        $this->address_city = $address_city;
    }

    /**
     * @return null|string
     */
    public function getAddressCountry()
    {
        return $this->address_country;
    }

    /**
     * @param null|string $address_country
     */
    public function setAddressCountry($address_country)
    {
        $this->address_country = $address_country;
    }

    /**
     * @return null|string
     */
    public function getAddressLine1()
    {
        return $this->address_line1;
    }

    /**
     * @param null|string $address_line1
     */
    public function setAddressLine1($address_line1)
    {
        $this->address_line1 = $address_line1;
    }

    /**
     * @return null|string
     */
    public function getAddressLine1Check()
    {
        return $this->address_line1_check;
    }

    /**
     * @param null|string $address_line1_check
     */
    public function setAddressLine1Check($address_line1_check)
    {
        $this->address_line1_check = $address_line1_check;
    }

    /**
     * @return null|string
     */
    public function getAddressLine2()
    {
        return $this->address_line2;
    }

    /**
     * @param null|string $address_line2
     */
    public function setAddressLine2($address_line2)
    {
        $this->address_line2 = $address_line2;
    }

    /**
     * @return null|string
     */
    public function getAddressState()
    {
        return $this->address_state;
    }

    /**
     * @param null|string $address_state
     */
    public function setAddressState($address_state)
    {
        $this->address_state = $address_state;
    }

    /**
     * @return null|string
     */
    public function getAddressZip()
    {
        return $this->address_zip;
    }

    /**
     * @param null|string $address_zip
     */
    public function setAddressZip($address_zip)
    {
        $this->address_zip = $address_zip;
    }

    /**
     * @return null|string
     */
    public function getAddressZipCheck()
    {
        return $this->address_zip_check;
    }

    /**
     * @param null|string $address_zip_check
     */
    public function setAddressZipCheck($address_zip_check)
    {
        $this->address_zip_check = $address_zip_check;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return null|string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param null|string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param string $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return null|string
     */
    public function getCvcCheck()
    {
        return $this->cvc_check;
    }

    /**
     * @param null|string $cvc_check
     */
    public function setCvcCheck($cvc_check)
    {
        $this->cvc_check = $cvc_check;
    }

    /**
     * @return bool|null
     */
    public function getDefaultForCurrency()
    {
        return $this->default_for_currency;
    }

    /**
     * @param bool|null $default_for_currency
     */
    public function setDefaultForCurrency($default_for_currency)
    {
        $this->default_for_currency = $default_for_currency;
    }

    /**
     * @return null|string
     */
    public function getDynamicLast4()
    {
        return $this->dynamic_last4;
    }

    /**
     * @param null|string $dynamic_last4
     */
    public function setDynamicLast4($dynamic_last4)
    {
        $this->dynamic_last4 = $dynamic_last4;
    }

    /**
     * @return int
     */
    public function getExpMonth()
    {
        return $this->exp_month;
    }

    /**
     * @param int $exp_month
     */
    public function setExpMonth($exp_month)
    {
        $this->exp_month = $exp_month;
    }

    /**
     * @return int
     */
    public function getExpYear()
    {
        return $this->exp_year;
    }

    /**
     * @param int $exp_year
     */
    public function setExpYear($exp_year)
    {
        $this->exp_year = $exp_year;
    }

    /**
     * @return null|string
     */
    public function getFingerprint()
    {
        return $this->fingerprint;
    }

    /**
     * @param null|string $fingerprint
     */
    public function setFingerprint($fingerprint)
    {
        $this->fingerprint = $fingerprint;
    }

    /**
     * @return string
     */
    public function getFunding()
    {
        return $this->funding;
    }

    /**
     * @param string $funding
     */
    public function setFunding($funding)
    {
        $this->funding = $funding;
    }

    /**
     * @return string
     */
    public function getLast4()
    {
        return $this->last4;
    }

    /**
     * @param string $last4
     */
    public function setLast4($last4)
    {
        $this->last4 = $last4;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param null|string $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return null|string
     */
    public function getTokenizationMethod()
    {
        return $this->tokenization_method;
    }

    /**
     * @param null|string $tokenization_method
     */
    public function setTokenizationMethod($tokenization_method)
    {
        $this->tokenization_method = $tokenization_method;
    }
    
}