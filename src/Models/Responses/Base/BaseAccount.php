<?php

namespace jamesvweston\Stripe\Models\Responses\Base;


use jamesvweston\Stripe\Models\Responses\Contracts\Account AS AccountContract;

/**
 * Class BaseAccount
 * @see     https://stripe.com/docs/api#account_object
 * @package jamesvweston\Stripe\Models\Responses\Base
 */
abstract class BaseAccount implements AccountContract
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $object           = 'account';

    /**
     * The publicly visible name of the business
     * @var string
     */
    protected $business_name;

    /**
     * A CSS hex color value representing the primary branding color for this account
     * @var string
     */
    protected $business_primary_color;

    /**
     * The publicly visible website of the business
     * @var string|null
     */
    protected $business_url;

    /**
     * Whether or not the account can create live charges
     * @var bool
     */
    protected $charges_enabled;

    /**
     * The country of the account
     * @var string
     */
    protected $country;

    /**
     * MANAGED ACCOUNTS ONLY
     * Whether or not Stripe will attempt to reclaim negative account balances from this account’s bank account.
     * @var bool
     */
    protected $debit_negative_balances;

    /**
     * @see https://stripe.com/docs/api#account_object-decline_charge_on
     * Account-level settings to automatically decline certain types of charges regardless of the bank’s decision
     * @var BaseDeclineChargeOn
     */
    protected $decline_charge_on;

    /**
     * The currency this account has chosen to use as the default
     * @var string
     */
    protected $default_currency;

    /**
     * Whether or not account details have been submitted yet. Standalone accounts cannot receive transfers before this is true
     * @var bool
     */
    protected $details_submitted;

    /**
     * The display name for this account. This is used on the Stripe dashboard to help you differentiate between accounts
     * @var string
     */
    protected $display_name;

    /**
     * The primary user’s email address
     * @var string
     */
    protected $email;

    /**
     * MANAGED ACCOUNTS ONLY
     * @var array|null
     */
    protected $external_accounts;

    /**
     * MANAGED ACCOUNTS ONLY
     * Information regarding the owner of this account, including verification status
     * @var BaseLegalEntity|null
     */
    protected $legal_entity;

    /**
     * Whether or not the account is managed by your platform
     * Returns null if the account was not created by a platform.
     * @var bool|null
     */
    protected $managed;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * MANAGED ACCOUNTS ONLY
     * An internal-only description of the product or service provided
     * This is used by Stripe in the event the account gets flagged for potential fraud
     * @var string
     */
    protected $product_description;

    /**
     * The text that will appear on credit card statements
     * @var string
     */
    protected $statement_descriptor;

    /**
     * A publicly shareable email address that can be reached for support for this account
     * @var string
     */
    protected $support_email;

    /**
     * The publicly visible support phone number for the business
     * @var string
     */
    protected $support_phone;

    /**
     * A publicly shareable URL that can be reached for support for this account
     * @var string
     */
    protected $support_url;

    /**
     * The timezone used in the Stripe dashboard for this account
     * @var string
     */
    protected $timezone;

    /**
     * MANAGED ACCOUNTS ONLY
     * Who accepted the Stripe terms of service, and when they accepted it
     * @var BaseTOSAcceptance|null
     */
    protected $tos_acceptance;

    /**
     * MANAGED ACCOUNTS ONLY
     * @var BaseTransferSchedule|null
     */
    protected $transfer_schedule;

    /**
     * MANAGED ACCOUNTS ONLY
     * Whether or not Stripe will send automatic transfers for this account
     * This is only false when Stripe is waiting for additional information from the account holder
     * @var bool
     */
    protected $transfers_enabled;

    /**
     * MANAGED ACCOUNTS ONLY
     * The state of the account’s information requests
     * Including what information is needed and by when it must be provided
     * @var BaseVerification|null
     */
    protected $verification;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return string
     */
    public function getBusinessName()
    {
        return $this->business_name;
    }

    /**
     * @param string $business_name
     */
    public function setBusinessName($business_name)
    {
        $this->business_name = $business_name;
    }

    /**
     * @return string
     */
    public function getBusinessPrimaryColor()
    {
        return $this->business_primary_color;
    }

    /**
     * @param string $business_primary_color
     */
    public function setBusinessPrimaryColor($business_primary_color)
    {
        $this->business_primary_color = $business_primary_color;
    }

    /**
     * @return null|string
     */
    public function getBusinessUrl()
    {
        return $this->business_url;
    }

    /**
     * @param null|string $business_url
     */
    public function setBusinessUrl($business_url)
    {
        $this->business_url = $business_url;
    }

    /**
     * @return boolean
     */
    public function getChargesEnabled()
    {
        return $this->charges_enabled;
    }

    /**
     * @param boolean $charges_enabled
     */
    public function setChargesEnabled($charges_enabled)
    {
        $this->charges_enabled = $charges_enabled;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return boolean
     */
    public function getDebitNegativeBalances()
    {
        return $this->debit_negative_balances;
    }

    /**
     * @param boolean $debit_negative_balances
     */
    public function setDebitNegativeBalances($debit_negative_balances)
    {
        $this->debit_negative_balances = $debit_negative_balances;
    }

    /**
     * @return BaseDeclineChargeOn
     */
    public function getDeclineChargeOn()
    {
        return $this->decline_charge_on;
    }

    /**
     * @param BaseDeclineChargeOn $decline_charge_on
     */
    public function setDeclineChargeOn($decline_charge_on)
    {
        $this->decline_charge_on = $decline_charge_on;
    }

    /**
     * @return string
     */
    public function getDefaultCurrency()
    {
        return $this->default_currency;
    }

    /**
     * @param string $default_currency
     */
    public function setDefaultCurrency($default_currency)
    {
        $this->default_currency = $default_currency;
    }

    /**
     * @return boolean
     */
    public function getDetailsSubmitted()
    {
        return $this->details_submitted;
    }

    /**
     * @param boolean $details_submitted
     */
    public function setDetailsSubmitted($details_submitted)
    {
        $this->details_submitted = $details_submitted;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * @param string $display_name
     */
    public function setDisplayName($display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return array
     */
    public function getExternalAccounts()
    {
        return $this->external_accounts;
    }

    /**
     * @param array $external_accounts
     */
    public function setExternalAccounts($external_accounts)
    {
        $this->external_accounts = $external_accounts;
    }

    /**
     * @return BaseLegalEntity|null
     */
    public function getLegalEntity()
    {
        return $this->legal_entity;
    }

    /**
     * @param BaseLegalEntity|null $legal_entity
     */
    public function setLegalEntity($legal_entity)
    {
        $this->legal_entity = $legal_entity;
    }
    

    /**
     * @return bool|null
     */
    public function getManaged()
    {
        return $this->managed;
    }

    /**
     * @param bool|null $managed
     */
    public function setManaged($managed)
    {
        $this->managed = $managed;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return string
     */
    public function getProductDescription()
    {
        return $this->product_description;
    }

    /**
     * @param string $product_description
     */
    public function setProductDescription($product_description)
    {
        $this->product_description = $product_description;
    }

    /**
     * @return string
     */
    public function getStatementDescriptor()
    {
        return $this->statement_descriptor;
    }

    /**
     * @param string $statement_descriptor
     */
    public function setStatementDescriptor($statement_descriptor)
    {
        $this->statement_descriptor = $statement_descriptor;
    }

    /**
     * @return string
     */
    public function getSupportEmail()
    {
        return $this->support_email;
    }

    /**
     * @param string $support_email
     */
    public function setSupportEmail($support_email)
    {
        $this->support_email = $support_email;
    }

    /**
     * @return string
     */
    public function getSupportPhone()
    {
        return $this->support_phone;
    }

    /**
     * @param string $support_phone
     */
    public function setSupportPhone($support_phone)
    {
        $this->support_phone = $support_phone;
    }

    /**
     * @return string
     */
    public function getSupportUrl()
    {
        return $this->support_url;
    }

    /**
     * @param string $support_url
     */
    public function setSupportUrl($support_url)
    {
        $this->support_url = $support_url;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @return BaseTOSAcceptance|null
     */
    public function getTosAcceptance()
    {
        return $this->tos_acceptance;
    }

    /**
     * @param BaseTOSAcceptance|null $tos_acceptance
     */
    public function setTosAcceptance($tos_acceptance)
    {
        $this->tos_acceptance = $tos_acceptance;
    }

    /**
     * @return BaseTransferSchedule|null
     */
    public function getTransferSchedule()
    {
        return $this->transfer_schedule;
    }

    /**
     * @param BaseTransferSchedule|null $transfer_schedule
     */
    public function setTransferSchedule($transfer_schedule)
    {
        $this->transfer_schedule = $transfer_schedule;
    }
    
    /**
     * @return boolean
     */
    public function getTransfersEnabled()
    {
        return $this->transfers_enabled;
    }

    /**
     * @param boolean $transfers_enabled
     */
    public function setTransfersEnabled($transfers_enabled)
    {
        $this->transfers_enabled = $transfers_enabled;
    }

    /**
     * @return BaseVerification|null
     */
    public function getVerification()
    {
        return $this->verification;
    }

    /**
     * @param BaseVerification|null $verification
     */
    public function setVerification($verification)
    {
        $this->verification = $verification;
    }
    
}