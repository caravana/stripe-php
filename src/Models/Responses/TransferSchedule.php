<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseTransferSchedule;
use jamesvweston\Utilities\ArrayUtil AS AU;

class TransferSchedule extends BaseTransferSchedule
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->delay_days               = AU::get($data['delay_days']);
            $this->interval                 = AU::get($data['interval']);
            $this->monthly_anchor           = AU::get($data['monthly_anchor']);
            $this->weekly_anchor            = AU::get($data['weekly_anchor']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['delay_days']               = $this->delay_days;
        $object['interval']                 = $this->interval;
        $object['monthly_anchor']           = $this->monthly_anchor;
        $object['weekly_anchor']            = $this->weekly_anchor;

        return $object;
    }

}