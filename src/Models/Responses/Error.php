<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseError;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Error extends BaseError
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->type                     = AU::get($data['type']);
            $this->message                  = AU::get($data['message']);
            $this->param                    = AU::get($data['param']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['type']                     = $this->type;
        $object['message']                  = $this->message;
        $object['param']                    = $this->param;

        return $object;
    }
    
}