<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseAddress;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Address extends BaseAddress
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->city                 = AU::get($data['city']);
            $this->country              = AU::get($data['country']);
            $this->line1                = AU::get($data['line1']);
            $this->line2                = AU::get($data['line2']);
            $this->postal_code          = AU::get($data['postal_code']);
            $this->state                = AU::get($data['state']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['city']                 = $this->city;
        $object['country']              = $this->country;
        $object['line1']                = $this->line1;
        $object['line2']                = $this->line2;
        $object['postal_code']          = $this->postal_code;
        $object['state']                = $this->state;

        return $object;
    }
    
}