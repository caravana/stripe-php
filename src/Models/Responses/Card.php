<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseCard;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Card extends BaseCard
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            if (!is_null(AU::get($data['account'])))
                $this->account              = new Account(AU::get($data['account']));
            
            $this->address_city             = AU::get($data['address_city']);
            $this->address_country          = AU::get($data['address_country']);
            $this->address_line1            = AU::get($data['address_line1']);
            $this->address_line1_check      = AU::get($data['address_line1_check']);
            $this->address_line2            = AU::get($data['address_line2']);
            $this->address_state            = AU::get($data['address_state']);
            $this->address_zip              = AU::get($data['address_zip']);
            $this->address_zip_check        = AU::get($data['address_zip_check']);
            $this->brand                    = AU::get($data['brand']);
            $this->country                  = AU::get($data['country']);
            $this->currency                 = AU::get($data['currency']);
            $this->customer                 = AU::get($data['customer']);
            $this->cvc_check                = AU::get($data['cvc_check']);
            $this->default_for_currency     = AU::get($data['default_for_currency']);
            $this->dynamic_last4            = AU::get($data['dynamic_last4']);
            $this->exp_month                = AU::get($data['exp_month']);
            $this->exp_year                 = AU::get($data['exp_year']);
            $this->fingerprint              = AU::get($data['fingerprint']);
            $this->funding                  = AU::get($data['funding']);
            $this->last4                    = AU::get($data['last4']);
            $this->metadata                 = AU::get($data['metadata']);
            $this->name                     = AU::get($data['name']);
            $this->recipient                = AU::get($data['recipient']);
            $this->tokenization_method      = AU::get($data['tokenization_method']);
        }
    }


    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['object']                   = $this->object;
        $object['account']                  = ($this->account instanceof \JsonSerializable) ? $this->account->jsonSerialize() : null;
        $object['address_city']             = $this->address_city;
        $object['address_country']          = $this->address_country;
        $object['address_line1']            = $this->address_line1;
        $object['address_line1_check']      = $this->address_line1_check;
        $object['address_line2']            = $this->address_line2;
        $object['address_state']            = $this->address_state;
        $object['address_zip']              = $this->address_zip;
        $object['address_zip_check']        = $this->address_zip_check;
        $object['brand']                    = $this->brand;
        $object['country']                  = $this->country;
        $object['currency']                 = $this->currency;
        $object['customer']                 = $this->customer;
        $object['cvc_check']                = $this->cvc_check;
        $object['default_for_currency']     = $this->default_for_currency;
        $object['dynamic_last4']            = $this->dynamic_last4;
        $object['exp_month']                = $this->exp_month;
        $object['exp_year']                 = $this->exp_year;
        $object['fingerprint']              = $this->fingerprint;
        $object['funding']                  = $this->funding;
        $object['last4']                    = $this->last4;
        $object['metadata']                 = $this->metadata;
        $object['name']                     = $this->name;
        $object['recipient']                = $this->recipient;
        $object['tokenization_method']      = $this->tokenization_method;

        return $object;
    }

}