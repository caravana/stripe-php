<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseShipping;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Shipping extends BaseShipping
{

    public function __construct($data = null)
    {
        if (is_array($data))
        {
            if (!is_null(AU::get($data['address'])))
                $this->address          = new Address($data['address']);
            
            $this->name                 = AU::get($data['name']);
            $this->phone                = AU::get($data['phone']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['address']              = ($this->address instanceof \JsonSerializable) ? $this->address->jsonSerialize() : null;
        $object['name']                 = $this->name;
        $object['phone']                = $this->phone;
        
        return $object;
    }
}