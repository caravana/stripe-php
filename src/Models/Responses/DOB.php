<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseDOB;
use jamesvweston\Utilities\ArrayUtil AS AU;

class DOB extends BaseDOB
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->day                  = AU::get($data['day']);
            $this->month                = AU::get($data['month']);
            $this->year                 = AU::get($data['year']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['day']                  = $this->day;
        $object['month']                = $this->month;
        $object['year']                 = $this->year;

        return $object;
    }
    
}