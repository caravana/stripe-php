<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseAccount;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Account extends BaseAccount
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->object                   = AU::get($data['object']);
            $this->business_name            = AU::get($data['business_name']);
            $this->business_primary_color   = AU::get($data['business_primary_color']);
            $this->business_url             = AU::get($data['business_url']);
            $this->charges_enabled          = AU::get($data['charges_enabled']);
            $this->country                  = AU::get($data['country']);
            $this->debit_negative_balances  = AU::get($data['debit_negative_balances']);
            $this->decline_charge_on        = !is_null(AU::get($data['decline_charge_on'])) ? new DeclineChargeOn(AU::get($data['decline_charge_on'])) : null;
            $this->default_currency         = AU::get($data['default_currency']);
            $this->details_submitted        = AU::get($data['details_submitted']);
            $this->display_name             = AU::get($data['display_name']);
            $this->email                    = AU::get($data['email']);
            $this->external_accounts        = AU::get($data['external_accounts']);
            $this->legal_entity             = !is_null(AU::get($data['legal_entity'])) ? new LegalEntity(AU::get($data['legal_entity'])) : null;
            $this->managed                  = AU::get($data['managed']);
            $this->metadata                 = AU::get($data['metadata']);
            $this->product_description      = AU::get($data['product_description']);
            $this->statement_descriptor     = AU::get($data['statement_descriptor']);
            $this->support_email            = AU::get($data['support_email']);
            $this->support_phone            = AU::get($data['support_phone']);
            $this->support_url              = AU::get($data['support_url']);
            $this->timezone                 = AU::get($data['timezone']);
            $this->tos_acceptance           = !is_null(AU::get($data['tos_acceptance'])) ? new TOSAcceptance(AU::get($data['tos_acceptance'])) : null;
            $this->transfer_schedule        = !is_null(AU::get($data['transfer_schedule'])) ? new TransferSchedule(AU::get($data['transfer_schedule'])) : null;
            $this->transfers_enabled        = AU::get($data['transfers_enabled']);
            $this->verification             = !is_null(AU::get($data['verification'])) ? new Verification(AU::get($data['verification'])) : null;
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['object']                   = $this->object;
        $object['business_name']            = $this->business_name;
        $object['business_primary_color']   = $this->business_primary_color;
        $object['business_url']             = $this->business_url;
        $object['charges_enabled']          = $this->charges_enabled;
        $object['country']                  = $this->country;
        $object['debit_negative_balances']  = $this->debit_negative_balances;
        $object['decline_charge_on']        = ($this->decline_charge_on instanceof \JsonSerializable) ? $this->decline_charge_on->jsonSerialize() : null;
        $object['default_currency']         = $this->default_currency;
        $object['details_submitted']        = $this->details_submitted;
        $object['display_name']             = $this->display_name;
        $object['email']                    = $this->email;
        $object['external_accounts']        = $this->external_accounts;
        $object['legal_entity']             = ($this->legal_entity instanceof \JsonSerializable) ? $this->legal_entity->jsonSerialize() : null;
        $object['managed']                  = $this->managed;
        $object['metadata']                 = $this->metadata;
        $object['product_description']      = $this->product_description;
        $object['statement_descriptor']     = $this->statement_descriptor;
        $object['support_email']            = $this->support_email;
        $object['support_phone']            = $this->support_phone;
        $object['support_url']              = $this->support_url;
        $object['timezone']                 = $this->timezone;
        $object['tos_acceptance']           = ($this->tos_acceptance instanceof \JsonSerializable) ? $this->tos_acceptance->jsonSerialize() : null;
        $object['transfer_schedule']        = ($this->transfer_schedule instanceof \JsonSerializable) ? $this->transfer_schedule->jsonSerialize() : null;
        $object['transfers_enabled']        = $this->transfers_enabled;
        $object['verification']             = ($this->verification instanceof \JsonSerializable) ? $this->verification->jsonSerialize() : null;

        return $object;
    }

}