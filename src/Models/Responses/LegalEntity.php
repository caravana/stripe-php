<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseLegalEntity;
use jamesvweston\Utilities\ArrayUtil AS AU;

class LegalEntity extends BaseLegalEntity
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->additional_owners        = AU::get($data['additional_owners']);
            $this->address                  = !is_null(AU::get($data['address'])) ? new Address(AU::get($data['address'])) : null;
            $this->business_name            = AU::get($data['business_name']);
            $this->business_tax_id_provided = AU::get($data['business_tax_id_provided']);
            $this->dob                      = !is_null(AU::get($data['dob'])) ? new DOB(AU::get($data['dob'])) : null;
            $this->first_name               = AU::get($data['first_name']);
            $this->last_name                = AU::get($data['last_name']);
            $this->personal_address         = !is_null(AU::get($data['personal_address'])) ? new Address(AU::get($data['personal_address'])) : null;
            $this->personal_id_number_provided = AU::get($data['personal_id_number_provided']);
            $this->phone_number             = AU::get($data['phone_number']);
            $this->ssn_last_4_provided      = AU::get($data['ssn_last_4_provided']);
            $this->type                     = AU::get($data['type']);
            $this->verification             = !is_null(AU::get($data['verification'])) ? new LegalEntityVerification(AU::get($data['verification'])) : null;
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['additional_owners']        = $this->additional_owners;
        $object['address']                  = ($this->address instanceof \JsonSerializable) ? $this->address->jsonSerialize() : null;
        $object['business_name']            = $this->business_name;
        $object['business_tax_id_provided'] = $this->business_tax_id_provided;
        $object['dob']                      = ($this->dob instanceof \JsonSerializable) ? $this->dob->jsonSerialize() : null;
        $object['first_name']               = $this->first_name;
        $object['last_name']                = $this->last_name;
        $object['personal_address']         = ($this->personal_address instanceof \JsonSerializable) ? $this->personal_address->jsonSerialize() : null;
        $object['personal_id_number_provided'] = $this->personal_id_number_provided;
        $object['phone_number']             = $this->phone_number;
        $object['ssn_last_4_provided']      = $this->ssn_last_4_provided;
        $object['type']                     = $this->type;
        $object['verification']             = ($this->verification instanceof \JsonSerializable) ? $this->verification->jsonSerialize() : null;

        return $object;
    }
}