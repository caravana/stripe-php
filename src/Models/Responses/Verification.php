<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseVerification;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Verification extends BaseVerification
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->disabled_reason          = AU::get($data['disabled_reason']);
            $this->due_by                   = AU::get($data['due_by']);
            $this->fields_needed            = AU::get($data['fields_needed']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['disabled_reason']          = $this->disabled_reason;
        $object['due_by']                   = $this->due_by;
        $object['fields_needed']            = $this->fields_needed;

        return $object;
    }
    
}