<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseRefund;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Refund extends BaseRefund
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                   = AU::get($data['id']);
            $this->object               = AU::get($data['object']);
            $this->amount               = AU::get($data['amount']);
            $this->balance_transaction  = AU::get($data['balance_transaction']);
            $this->charge               = AU::get($data['charge']);
            $this->created              = AU::get($data['created']);
            $this->currency             = AU::get($data['currency']);
            $this->description          = AU::get($data['description']);
            $this->metadata             = AU::get($data['metadata']);
            $this->reason               = AU::get($data['reason']);
            $this->receipt_number       = AU::get($data['receipt_number']);
            $this->status               = AU::get($data['status']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                   = $this->id;
        $object['object']               = $this->object;
        $object['amount']               = $this->amount;
        $object['balance_transaction']  = $this->balance_transaction;
        $object['charge']               = $this->charge;
        $object['created']              = $this->created;
        $object['currency']             = $this->currency;
        $object['description']          = $this->description;
        $object['metadata']             = $this->metadata;
        $object['reason']               = $this->reason;
        $object['receipt_number']       = $this->receipt_number;
        $object['status']               = $this->status;

        return $object;
    }
    
}