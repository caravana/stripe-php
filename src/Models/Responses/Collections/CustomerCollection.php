<?php

namespace jamesvweston\Stripe\Models\Responses\Collections;


use jamesvweston\Stripe\Models\Responses\Base\BaseCollection;
use jamesvweston\Stripe\Models\Responses\Customer;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CustomerCollection extends BaseCollection
{

    /**
     * CustomerCollection constructor.
     * @param array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->has_more                 = AU::get($data['has_more']);
            $this->total_count              = AU::get($data['total_count']);
            $this->url                      = AU::get($data['url']);
            $this->setData(AU::get($data['data']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['data']                     = [];
        foreach ($this->getData() AS $item)
        {
            $object['data'][]               = $item->jsonSerialize();
        }

        $object['has_more']                 = $this->has_more;
        $object['total_count']              = $this->total_count;
        $object['url']                      = $this->url;

        return $object;
    }

    /**
     * @return Customer[]
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data             = [];

        if (is_array($data))
        {
            foreach ($data AS $item)
            {
                $this->data[]   = new Customer($item);
            }
        }
    }
    
}