<?php

namespace jamesvweston\Stripe\Models\Responses;
use jamesvweston\Stripe\Models\Responses\Base\BaseTOSAcceptance;
use jamesvweston\Utilities\ArrayUtil AS AU;

class TOSAcceptance extends BaseTOSAcceptance
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->date                     = AU::get($data['date']);
            $this->ip                       = AU::get($data['ip']);
            $this->user_agent               = AU::get($data['user_agent']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['date']                     = $this->date;
        $object['ip']                       = $this->ip;
        $object['user_agent']               = $this->user_agent;

        return $object;
    }
    
}