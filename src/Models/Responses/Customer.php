<?php

namespace jamesvweston\Stripe\Models\Responses;

use jamesvweston\Stripe\Models\Responses\Base\BaseCustomer;
use jamesvweston\Utilities\ArrayUtil AS AU;


class Customer extends BaseCustomer
{

    
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->account_balance          = AU::get($data['account_balance']);
            $this->business_vat_id          = AU::get($data['business_vat_id']);
            $this->created                  = AU::get($data['created']);
            $this->currency                 = AU::get($data['currency']);
            $this->default_source           = AU::get($data['default_source']);

            $this->delinquent               = AU::get($data['delinquent']);
            $this->description              = AU::get($data['description']);
            
            if (!is_null(AU::get($data['discount'])))
                $this->discount             = new Discount($data['discount']);
            
            $this->email                    = AU::get($data['email']);
            $this->liveMode                 = AU::get($data['liveMode']);
            $this->metadata                 = AU::get($data['metadata']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['account_balance']          = $this->account_balance;
        $object['business_vat_id']          = $this->business_vat_id;
        $object['created']                  = $this->created;
        $object['currency']                 = $this->currency;
        $object['default_source']           = $this->default_source;

        $object['delinquent']               = $this->delinquent;
        $object['description']              = $this->description;
        $object['discount']                 = ($this->discount instanceof \JsonSerializable) ? $this->discount->jsonSerialize() : null;
        
        $object['email']                    = $this->email;
        $object['liveMode']                 = $this->liveMode;
        $object['metadata']                 = $this->metadata;
        
        return $object;
    }

    /**
     * @return array
     */
    public function jsonSerializeUpdate()
    {
        $object                             = $this->jsonSerialize();
        
        unset($object['id']);
        unset($object['created']);
        unset($object['delinquent']);
        
        return $object;
    }

    
    
}