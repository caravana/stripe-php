<?php

namespace jamesvweston\Stripe\Models\Responses;
use jamesvweston\Stripe\Models\Responses\Base\BaseFeeDetail;
use jamesvweston\Utilities\ArrayUtil AS AU;

/**
 * Class FeeDetail
 * 
 * @package jamesvweston\Stripe\Models
 */
class FeeDetail extends BaseFeeDetail
{


    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->amount                   = AU::get($data['amount']);
            $this->application              = AU::get($data['application']);
            $this->currency                 = AU::get($data['currency']);
            $this->description              = AU::get($data['description']);
            $this->type                     = AU::get($data['type']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['amount']                   = $this->amount;
        $object['application']              = $this->application;
        $object['currency']                 = $this->currency;
        $object['description']              = $this->description;
        $object['type']                     = $this->type;

        return $object;
    }

}