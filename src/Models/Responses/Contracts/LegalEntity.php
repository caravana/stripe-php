<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;

interface LegalEntity extends \JsonSerializable
{
    public function getAdditionalOwners();

    public function setAdditionalOwners($additional_owners);

    public function getAddress();

    public function setAddress($address);

    public function getBusinessName();

    public function setBusinessName($business_name);

    public function isBusinessTaxIdProvided();

    public function setBusinessTaxIdProvided($business_tax_id_provided);

    public function getDob();

    public function setDob($dob);

    public function getFirstName();

    public function setFirstName($first_name);

    public function getLastName();

    public function setLastName($last_name);

    public function getPersonalAddress();

    public function setPersonalAddress($personal_address);

    public function isPersonalIdNumberProvided();

    public function setPersonalIdNumberProvided($personal_id_number_provided);

    public function getPhoneNumber();

    public function setPhoneNumber($phone_number);

    public function isSsnLast4Provided();

    public function setSsnLast4Provided($ssn_last_4_provided);

    public function getType();

    public function setType($type);

    public function getVerification();

    public function setVerification($verification);
}