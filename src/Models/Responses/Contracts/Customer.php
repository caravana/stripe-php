<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Customer extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getAccountBalance();
    function setAccountBalance($account_balance);
    function getBusinessVatId();
    function setBusinessVatId($business_vat_id);
    
}