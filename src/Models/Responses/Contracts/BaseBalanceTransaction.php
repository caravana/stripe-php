<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;

interface BaseBalanceTransaction extends \JsonSerializable
{
    public function getId();

    public function setId($id);

    public function getObject();

    public function setObject($object);

    public function getAmount();

    public function setAmount($amount);

    public function getAvailableOn();

    public function setAvailableOn($available_on);

    public function getCreated();

    public function setCreated($created);

    public function getCurrency();

    public function setCurrency($currency);

    public function getDescription();

    public function setDescription($description);

    public function getFee();

    public function setFee($fee);

    public function getFeeDetails();

    public function setFeeDetails($fee_details);

    public function getNet();

    public function setNet($net);

    public function getSource();

    public function setSource($source);

    public function getSourcedTransfers();

    public function setSourcedTransfers($sourced_transfers);

    public function getStatus();

    public function setStatus($status);

    public function getType();

    public function setType($type);
}