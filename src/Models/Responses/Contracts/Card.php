<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Card extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getObject();
    function setObject($object);
    function getAccount();
    function setAccount($account);
    function getAddressCity();
    function setAddressCity($address_city);
    function getAddressCountry();
    function setAddressCountry($address_country);
    function getAddressLine1();
    function setAddressLine1($address_line1);
    function getAddressLine1Check();
    function setAddressLine1Check($address_line1_check);
    function getAddressLine2();
    function setAddressLine2($address_line2);
    function getAddressState();
    function setAddressState($address_state);
    function getAddressZip();
    function setAddressZip($address_zip);
    function getAddressZipCheck();
    function setAddressZipCheck($address_zip_check);
    function getBrand();
    function setBrand($brand);
    function getCountry();
    function setCountry($country);
    function getCurrency();
    function setCurrency($currency);
    function getCustomer();
    function setCustomer($customer);
    function getCvcCheck();
    function setCvcCheck($cvc_check);
    function getDefaultForCurrency();
    function setDefaultForCurrency($default_for_currency);
    function getDynamicLast4();
    function setDynamicLast4($dynamic_last4);
    function getExpMonth();
    function setExpMonth($exp_month);
    function getExpYear();
    function setExpYear($exp_year);
    function getFingerprint();
    function setFingerprint($fingerprint);
    function getFunding();
    function setFunding($funding);
    function getLast4();
    function setLast4($last4);
    function getMetadata();
    function setMetadata($metadata);
    function getName();
    function setName($name);
    function getRecipient();
    function setRecipient($recipient);
    function getTokenizationMethod();
    function setTokenizationMethod($tokenization_method);
}