<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface FeeDetail extends \JsonSerializable
{

    function getAmount();

    function setAmount($amount);

    function getApplication();

    function setApplication($application);

    function getCurrency();

    function setCurrency($currency);

    function getDescription();

    function setDescription($description);

    function getType();

    function setType($type);
}