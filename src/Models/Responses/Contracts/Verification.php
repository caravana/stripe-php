<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Verification extends \JsonSerializable
{
    function getDisabledReason();
    function setDisabledReason($disabled_reason);
    function getDueBy();
    function setDueBy($due_by);
    function getFieldsNeeded();
    function setFieldsNeeded($fields_needed);
}