<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface TOSAcceptance extends \JsonSerializable
{
    function getDate();
    function setDate($date);
    function getIp();
    function setIp($ip);
    function getUserAgent();
    function setUserAgent($user_agent);
}