<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Error extends \JsonSerializable
{
    function getType();
    function setType($type);
    function getMessage();
    function setMessage($message);
    function getParam();
    function setParam($param);
}