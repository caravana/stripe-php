<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;

interface Transfer extends \JsonSerializable
{
    public function getId();

    public function setId($id);

    public function getObject();

    public function setObject($object);

    public function getAmount();

    public function setAmount($amount);

    public function getAmountReversed();

    public function setAmountReversed($amount_reversed);

    public function getApplicationFee();

    public function setApplicationFee($application_fee);

    public function getBalanceTransaction();

    public function setBalanceTransaction($balance_transaction);

    public function getCreated();

    public function setCreated($created);

    public function getCurrency();

    public function setCurrency($currency);

    public function getDate();

    public function setDate($date);

    public function getDescription();

    public function setDescription($description);

    public function getDestination();

    public function setDestination($destination);

    public function getDestinationPayment();

    public function setDestinationPayment($destination_payment);

    public function getFailureCode();

    public function setFailureCode($failure_code);

    public function getFailureMessage();

    public function setFailureMessage($failure_message);

    public function isLivemode();

    public function setLivemode($livemode);

    public function getMetadata();

    public function setMetadata($metadata);

    public function getMethod();

    public function setMethod($method);

    public function getReversals();

    public function setReversals($reversals);

    public function isReversed();

    public function setReversed($reversed);

    public function getSourceTransaction();

    public function setSourceTransaction($source_transaction);

    public function getSourceType();

    public function setSourceType($source_type);

    public function getStatementDescriptor();

    public function setStatementDescriptor($statement_descriptor);

    public function getStatus();

    public function setStatus($status);

    public function getType();

    public function setType($type);
}