<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface TransferSchedule extends \JsonSerializable
{
    function getDelayDays();
    function setDelayDays($delay_days);
    function getInterval();
    function setInterval($interval);
    function getMonthlyAnchor();
    function setMonthlyAnchor($monthly_anchor);
    function getWeeklyAnchor();
    function setWeeklyAnchor($weekly_anchor);
}