<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface BankAccount extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getObject();
    function setObject($object);
    function getAccount();
    function setAccount($account);
    function getCustomer();
    function setCustomer($customer);
    function getAccountHolderName();
    function setAccountHolderName($account_holder_name);
    function getAccountHolderType();
    function setAccountHolderType($account_holder_type);
    function getBankName();
    function setBankName($bank_name);
    function getCountry();
    function setCountry($country);
    function getCurrency();
    function setCurrency($currency);
    function isDefaultForCurrency();
    function setDefaultForCurrency($default_for_currency);
    function getFingerprint();
    function setFingerprint($fingerprint);
    function getLast4();
    function setLast4($last4);
    function getMetadata();
    function setMetadata($metadata);
    function getRoutingNumber();
    function setRoutingNumber($routing_number);
    function getStatus();
    function setStatus($status);
}