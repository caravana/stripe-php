<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Charge extends \JsonSerializable
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @param string $id
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getObject();

    /**
     * @param string $object
     */
    public function setObject($object);

    /**
     * @return int
     */
    public function getAmount();

    /**
     * @param int $amount
     */
    public function setAmount($amount);

    /**
     * @return int
     */
    public function getAmountRefunded();

    /**
     * @param int $amount_refunded
     */
    public function setAmountRefunded($amount_refunded);

    /**
     * @return null|string
     */
    public function getApplicationFee();

    /**
     * @param null|string $application_fee
     */
    public function setApplicationFee($application_fee);

    /**
     * @return string
     */
    public function getBalanceTransaction();

    /**
     * @param string $balance_transaction
     */
    public function setBalanceTransaction($balance_transaction);

    /**
     * @return bool|null
     */
    public function getCaptured();

    /**
     * @param bool|null $captured
     */
    public function setCaptured($captured);

    /**
     * @return int
     */
    public function getCreated();

    /**
     * @param int $created
     */
    public function setCreated($created);

    /**
     * @return string
     */
    public function getCurrency();

    /**
     * @param string $currency
     */
    public function setCurrency($currency);

    /**
     * @return null|string
     */
    public function getCustomer();

    /**
     * @param null|string $customer
     */
    public function setCustomer($customer);

    /**
     * @return null|string
     */
    public function getDescription();

    /**
     * @param null|string $description
     */
    public function setDescription($description);

    /**
     * @return null|string
     */
    public function getDestination();

    /**
     * @param null|string $destination
     */
    public function setDestination($destination);

   
    public function getDispute();

    
    public function setDispute($dispute);

    /**
     * @return null|string
     */
    public function getFailureCode();

    /**
     * @param null|string $failure_code
     */
    public function setFailureCode($failure_code);

    /**
     * @return null|string
     */
    public function getFailureMessage();

    /**
     * @param null|string $failure_message
     */
    public function setFailureMessage($failure_message);

    /**
     * @return array|null
     */
    public function getFraudDetails();

    /**
     * @param array|null $fraud_details
     */
    public function setFraudDetails($fraud_details);

    /**
     * @return null|string
     */
    public function getInvoice();

    /**
     * @param null|string $invoice
     */
    public function setInvoice($invoice);

    /**
     * @return boolean
     */
    public function isLivemode();

    /**
     * @param boolean $livemode
     */
    public function setLivemode($livemode);

    /**
     * @return array
     */
    public function getMetadata();

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata);

    /**
     * @return null|string
     */
    public function getOrder();

    /**
     * @param null|string $order
     */
    public function setOrder($order);

    /**
     * @return boolean
     */
    public function isPaid();

    /**
     * @param boolean $paid
     */
    public function setPaid($paid);

    /**
     * @return null|string
     */
    public function getReceiptEmail();

    /**
     * @param null|string $receipt_email
     */
    public function setReceiptEmail($receipt_email);

    /**
     * @return null|string
     */
    public function getReceiptNumber();

    /**
     * @param null|string $receipt_number
     */
    public function setReceiptNumber($receipt_number);

    /**
     * @return boolean
     */
    public function isRefunded();

    /**
     * @param boolean $refunded
     */
    public function setRefunded($refunded);

    /**
     * @return array
     */
    public function getRefunds();

    /**
     * @param array $refunds
     */
    public function setRefunds($refunds);

    /**
     * @return array|null
     */
    public function getShipping();

    /**
     * @param array|null $shipping
     */
    public function setShipping($shipping);

    
    public function getSource();

  
    public function setSource($source);

    /**
     * @return null|string
     */
    public function getSourceTransfer();

    /**
     * @param null|string $source_transfer
     */
    public function setSourceTransfer($source_transfer);

    /**
     * @return null|string
     */
    public function getStatementDescriptor();

    /**
     * @param null|string $statement_descriptor
     */
    public function setStatementDescriptor($statement_descriptor);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $status
     */
    public function setStatus($status);

    /**
     * @return null|string
     */
    public function getTransfer();

    /**
     * @param null|string $transfer
     */
    public function setTransfer($transfer);
}