<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;

interface LegalEntityVerification extends \JsonSerializable
{
    public function getDetails();

    public function setDetails($details);

    public function getDetailsCode();

    public function setDetailsCode($details_code);

    public function getDocument();

    public function setDocument($document);

    public function getStatus();

    public function setStatus($status);
}