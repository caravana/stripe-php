<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Collection extends \JsonSerializable
{
    function getData();
    function setData($data);
    function getHasMore();
    function setHasMore($has_more);
    function getTotalCount();
    function setTotalCount($total_count);
    function getUrl();
    function setUrl($url);
}