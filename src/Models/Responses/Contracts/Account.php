<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Account extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getObject();
    function setObject($object);
    function getBusinessName();
    function setBusinessName($business_name);
    function getBusinessPrimaryColor();
    function setBusinessPrimaryColor($business_primary_color);
    function getBusinessUrl();
    function setBusinessUrl($business_url);
    function getChargesEnabled();
    function setChargesEnabled($charges_enabled);
    function getCountry();
    function setCountry($country);
    function getDebitNegativeBalances();
    function setDebitNegativeBalances($debit_negative_balances);
    function getDeclineChargeOn();
    function setDeclineChargeOn($decline_charge_on);
    function getDefaultCurrency();
    function setDefaultCurrency($default_currency);
    function getDetailsSubmitted();
    function setDetailsSubmitted($details_submitted);
    function getDisplayName();
    function setDisplayName($display_name);
    function getEmail();
    function setEmail($email);
    function getExternalAccounts();
    function setExternalAccounts($external_accounts);
    function getLegalEntity();
    function setLegalEntity($legal_entity);
    function getManaged();
    function setManaged($managed);
    function getMetadata();
    function setMetadata($metadata);
    function getProductDescription();
    function setProductDescription($product_description);
    function getStatementDescriptor();
    function setStatementDescriptor($statement_descriptor);
    function getSupportEmail();
    function setSupportEmail($support_email);
    function getSupportPhone();
    function setSupportPhone($support_phone);
    function getSupportUrl();
    function setSupportUrl($support_url);
    function getTimezone();
    function setTimezone($timezone);
    function getTosAcceptance();
    function setTosAcceptance($tos_acceptance);
    function getTransferSchedule();
    function setTransferSchedule($transfer_schedule);
    function getTransfersEnabled();
    function setTransfersEnabled($transfers_enabled);
    function getVerification();
    function setVerification($verification);
}