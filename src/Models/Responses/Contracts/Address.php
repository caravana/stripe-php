<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Address extends \JsonSerializable
{
    function getCity();
    function setCity($city);
    function getCountry();
    function setCountry($country);
    function getLine1();
    function setLine1($line1);
    function getLine2();
    function setLine2($line2);
    function getPostalCode();
    function setPostalCode($postal_code);
    function getState();
    function setState($state);
}