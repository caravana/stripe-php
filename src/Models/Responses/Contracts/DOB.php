<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;

interface DOB extends \JsonSerializable
{
    public function getDay();

    public function setDay($day);

    public function getMonth();

    public function setMonth($month);

    public function getYear();

    public function setYear($year);
}