<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface Refund extends \JsonSerializable
{
    public function getId();

    public function setId($id);

    public function getObject();

    public function setObject($object);

    public function getAmount();

    public function setAmount($amount);

    public function getBalanceTransaction();

    public function setBalanceTransaction($balance_transaction);

    public function getCharge();

    public function setCharge($charge);

    public function getCreated();

    public function setCreated($created);

    public function getCurrency();

    public function setCurrency($currency);

    public function getDescription();

    public function setDescription($description);

    public function getMetadata();

    public function setMetadata($metadata);

    public function getReason();

    public function setReason($reason);

    public function getReceiptNumber();

    public function setReceiptNumber($receipt_number);

    public function getStatus();

    public function setStatus($status);
}