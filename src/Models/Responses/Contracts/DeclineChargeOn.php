<?php

namespace jamesvweston\Stripe\Models\Responses\Contracts;


interface DeclineChargeOn extends \JsonSerializable
{
    function getAvsFailure();
    function setAvsFailure($avs_failure);
    function getCvcFailure();
    function setCvcFailure($cvc_failure);
}