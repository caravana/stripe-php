<?php

namespace jamesvweston\Stripe\Models\Responses;


use jamesvweston\Stripe\Models\Responses\Base\BaseBalanceTransaction;
use jamesvweston\Stripe\Models\Responses\Collections\TransferCollection;
use jamesvweston\Utilities\ArrayUtil AS AU;

class BalanceTransaction extends BaseBalanceTransaction
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->object                   = AU::get($data['object']);
            $this->amount                   = AU::get($data['amount']);
            $this->available_on             = AU::get($data['available_on']);
            $this->created                  = AU::get($data['created']);
            $this->currency                 = AU::get($data['currency']);
            $this->description              = AU::get($data['description']);
            $this->fee                      = AU::get($data['fee']);

            $this->fee_details              = [];
            if (!empty(AU::get($data['fee_details'])))
            {
                $feeDetails                 = AU::get($data['fee_details']);
                foreach ($feeDetails AS $item)
                    $this->fee_details[]    = new FeeDetail($item);
            }

            $this->net                      = AU::get($data['net']);
            $this->source                   = AU::get($data['source']);

            $this->sourced_transfers        = new TransferCollection(AU::get($data['sourced_transfers']));

            $this->status                   = AU::get($data['status']);
            $this->type                     = AU::get($data['type']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['object']                   = $this->object;
        $object['amount']                   = $this->amount;
        $object['available_on']             = $this->available_on;
        $object['created']                  = $this->created;
        $object['currency']                 = $this->currency;
        $object['description']              = $this->description;
        $object['fee']                      = $this->fee;

        $object['fee_details']              = [];
        foreach ($this->getFeeDetails() AS $item)
            $object['fee_details'][]        = $item->jsonSerialize();

        $object['net']                      = $this->net;
        $object['source']                   = $this->source;

        $object['sourced_transfers']        = $this->sourced_transfers->jsonSerialize();

        $object['status']                   = $this->status;
        $object['type']                     = $this->type;

        return $object;
    }
    
}