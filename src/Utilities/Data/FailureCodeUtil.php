<?php

namespace jamesvweston\Stripe\Utilities\Data;


class FailureCodeUtil
{
    /**
     * @param   string  $code
     * @return  string
     */
    public static function parseFailureCode($code)
    {
        if ($code == 'invalid_number')
            return 'The card number is not a valid credit card number';
        else if ($code == 'invalid_expiry_month')
            return 'The card\'s expiration month is invalid';
        else if ($code == 'invalid_expiry_year')
            return 'The card\'s expiration year is invalid';
        else if ($code == 'invalid_cvc')
            return 'The card\'s security code is invalid';
        else if ($code == 'incorrect_number')
            return 'The card number is incorrect';
        else if ($code == 'expired_card')
            return 'The card has expired';
        else if ($code == 'incorrect_cvc')
            return 'The card\'s security code is incorrect';
        else if ($code == 'incorrect_zip')
            return 'The card\'s zip code failed validation';
        else if ($code == 'card_declined')
            return 'The card was declined';
        else if ($code == 'missing')
            return 'There is no card on a customer that is being charged';
        else if ($code == 'processing_error')
            return 'An error occurred while processing the card';
        else
            return 'Error while processing payment';
    }
}