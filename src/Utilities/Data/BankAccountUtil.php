<?php

namespace jamesvweston\Stripe\Utilities\Data;

/**
 * @see https://stripe.com/docs/testing
 * Class BankAccountUtil
 * @package jamesvweston\Stripe\Utilities\Data
 */
class BankAccountUtil
{

    public static function validUSAccount()
    {
        return [
            'number'        => '000123456789',
            'routing'       => '110000000'
        ];
    }

}