<?php

namespace jamesvweston\Stripe\Utilities\Data;

/**
 * Class TransferFailureTypeDataUtil
 * @see     https://stripe.com/docs/api#transfer_failures
 * @package jamesvweston\Stripe\Utilities\Data
 */
class TransferFailureTypeDataUtil
{

    /**
     * Your Stripe account has insufficient funds to cover the transfer.
     * @var string
     */
    protected static $insufficient_funds    = 'insufficient_funds';

    /**
     * The bank account has been closed.
     * @var string
     */
    protected static $account_closed        = 'account_closed';

    /**
     * The bank account details on file are probably incorrect.
     * No bank account could be located with those details.
     * @var string
     */
    protected static $no_account            = 'no_account';

    /**
     * The routing number seems correct, but the account number is invalid.
     * @var string
     */
    protected static $invalid_account_number= 'invalid_account_number';

    /**
     * Debit transactions are not approved on the bank account.
     * Stripe requires bank accounts to be set up for both credit and debit transfers.
     * @var string
     */
    protected static $debit_not_authorized  = 'debit_not_authorized';

    /**
     * The destination bank account is no longer valid because its branch has changed ownership.
     * @var string
     */
    protected static $bank_ownership_changed= 'bank_ownership_changed';

    /**
     * The bank account has been frozen.
     * @var string
     */
    protected static $account_frozen        = 'account_frozen';

    /**
     * The bank could not process this transfer.
     * @var string
     */
    protected static $could_not_process     = 'could_not_process';

    /**
     * The bank account has restrictions on either the type or number of transfers allowed.
     * This normally indicates that the bank account is a savings or other non-checking account.
     * @var string
     */
    protected static $bank_account_restricted='bank_account_restricted';

    /**
     * The bank was unable to process this transfer because of its currency.
     * This is probably because the bank account cannot accept payments in that currency.
     * @var string
     */
    protected static $invalid_currency      = 'invalid_currency';
}