<?php

namespace jamesvweston\Stripe\Utilities\Data;


class CardBrandDataUtil
{

    /**
     * @return array
     */
    public static function getAll()
    {
        return [
            'Visa', 'American Express', 'MasterCard', 'Discover', 'JCB', 'Diners Club', 'Unknown'
        ];
    }
}