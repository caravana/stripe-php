<?php

namespace jamesvweston\Stripe\Utilities\Data;


class CardFundingDataUtil
{

    /**
     * @return array
     */
    public static function getAll()
    {
        return [
            'credit', 'debit', 'prepaid', 'unknown'
        ];
    }
}