<?php

namespace jamesvweston\Stripe\Api;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateBankAccountRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\CreateCustomerRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\ListCustomersRequest;
use jamesvweston\Stripe\Models\Responses\BankAccount;
use jamesvweston\Stripe\Models\Responses\Collections\CustomerCollection;
use jamesvweston\Stripe\Models\Responses\Customer;
use jamesvweston\Stripe\Models\Requests\Contracts\CreateCardRequest;
use jamesvweston\Stripe\Models\Responses\Card;
use jamesvweston\Utilities\ArrayUtil AS AU;

/**
 * Class CustomerApi
 * @see     https://stripe.com/docs/api#customers
 * @package jamesvweston\Stripe\Api
 */
class CustomerApi extends BaseApi
{

    /**
     * @see     https://stripe.com/docs/api#list_customers
     * @param   ListCustomersRequest|array $request
     * @return  CustomerCollection
     */
    public function index($request)
    {
        $data           = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        $result         = parent::makeHttpRequest('get', 'customers', $data);
        return new CustomerCollection($result);
    }

    /**
     * @see     https://stripe.com/docs/api#retrieve_customer
     * @param   string $id
     * @return  Customer
     */
    public function show($id)
    {
        $result         = parent::makeHttpRequest('get', 'customers/' . $id);
        return new Customer($result);
    }
    
    /**
     * @see     https://stripe.com/docs/api#create_customer
     * @param   CreateCustomerRequest|array $request
     * @return  Customer
     * @throws  \Exception
     */
    public function store($request)
    {
        $data           = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        $result         = parent::makeHttpRequest('post', 'customers', $data);
        return new Customer($result);
    }
    
    /**
     * @see     https://stripe.com/docs/api#update_customer
     * @param   Customer $request
     * @return  Customer
     */
    public function update($request)
    {
        $result         = parent::makeHttpRequest('post', 'customers/' . $request->getId(), $request->jsonSerializeUpdate());
        return new Customer($result);
    }

    /**
     * @see     https://stripe.com/docs/api#delete_customer
     * @param   string      $id
     * @return  bool
     */
    public function delete($id)
    {
        $result         = parent::makeHttpRequest('delete', 'customers/' . $id);
        if (AU::get($result['deleted']) == 1)
            return true;
        else
            return false;
    }

    /**
     * @see     https://stripe.com/docs/api#create_card
     * @param   string                      $id
     * @param   CreateCardRequest|array     $request
     * @return  Card
     */
    public function storeCard($id, $request)
    {
        $path                       = 'customers/' . $id . '/sources';
        $data                       = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        $result                     = parent::makeHttpRequest('post', $path, $data);
        return new Card($result);
    }

    /**
     * @see     https://stripe.com/docs/api#account_create_bank_account
     * @param   string                      $id
     * @param   CreateBankAccountRequest|array     $request
     * @return  BankAccount
     */
    public function storeBankAccount($id, $request)
    {
        $path                       = 'customers/' . $id . '/sources';
        $data                       = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        $result                     = parent::makeHttpRequest('post', $path, $data);
        return new BankAccount($result);
    }

}