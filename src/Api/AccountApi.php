<?php

namespace jamesvweston\Stripe\Api;


use jamesvweston\Stripe\Exceptions\StripeException;
use jamesvweston\Stripe\Models\Requests\Contracts\CreateAccountRequest;
use jamesvweston\Stripe\Models\Requests\Contracts\CreateBankAccountRequest;
use jamesvweston\Stripe\Models\Responses\Account;
use jamesvweston\Stripe\Models\Responses\BankAccount;
use jamesvweston\Utilities\ArrayUtil AS AU;

/**
 * @see https://stripe.com/docs/api#account
 * Class AccountApi
 * @package jamesvweston\Stripe\Api
 */
class AccountApi extends BaseApi
{

    /**
     * @see     https://stripe.com/docs/api#retrieve_account
     * If null is supplied your account will be returned
     * @param   string $id
     * @return  Account
     */
    public function show($id)
    {
        $result         = parent::makeHttpRequest('get', 'accounts/' . $id);
        return new Account($result);
    }

    /**
     * @see     https://stripe.com/docs/api#create_account
     * @param   CreateAccountRequest $request
     * @throws  StripeException
     * @return  Account
     */
    public function store($request)
    {
        $data           = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;

        $result         = parent::makeHttpRequest('post', 'accounts', $data);
        return new Account($result);
    }

    /**
     * @param   string              $id
     * @param   mixed               $request
     * @return  Account
     */
    public function update($id, $request)
    {
        $data           = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        
        $result         = parent::makeHttpRequest('post', 'accounts/' . $id, $data);
        return new Account($result);
    }

    /**
     * @see     https://stripe.com/docs/api#delete_account
     * @param   string      $id
     * @return  bool
     */
    public function delete($id)
    {
        $result         = parent::makeHttpRequest('delete', 'accounts/' . $id);
        if (AU::get($result['deleted']) == 1)
            return true;
        else
            return false;
    }

    /**
     * @see     https://stripe.com/docs/api#account_create_bank_account
     * @param   string                      $id
     * @param   CreateBankAccountRequest|array     $request
     * @return  BankAccount
     */
    public function storeBankAccount($id, $request)
    {
        $path                       = 'accounts/' . $id . '/external_accounts';
        $data                       = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        $result                     = parent::makeHttpRequest('post', $path, $data);
        return new BankAccount($result);
    }

    /**
     * @see     https://stripe.com/docs/api#account_retrieve_bank_account
     * @param   string $accountId
     * @param   string $bankAccountId
     * @return  BankAccount
     */
    public function showBankAccount($accountId, $bankAccountId)
    {
        $path                       = 'accounts/' . $accountId . '/external_accounts/' . $bankAccountId;
        $result                     = parent::makeHttpRequest('get', $path);
        return new BankAccount($result);
    }

    /**
     * @see     https://stripe.com/docs/api#account_delete_bank_account
     * @param   string $accountId
     * @param   string $bankAccountId
     * @return  bool
     */
    public function deleteBankAccount($accountId, $bankAccountId)
    {
        $path                       = 'accounts/' . $accountId . '/external_accounts/' . $bankAccountId;
        $result                     = parent::makeHttpRequest('delete', $path);
        if (AU::get($result['deleted']) == 1)
            return true;
        else
            return false;
    }

}