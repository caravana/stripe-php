<?php

namespace jamesvweston\Stripe\Api;

use jamesvweston\Stripe\Models\Requests\Contracts\CreateChargeRequest;
use jamesvweston\Stripe\Models\Responses\Charge;

/**
 * @see https://stripe.com/docs/api#charges
 * Class ChargeApi
 * @package jamesvweston\Stripe\Api
 */
class ChargeApi extends BaseApi
{

    /**
     * @see     https://stripe.com/docs/api#retrieve_charge
     * @param   string $id
     * @return  Charge
     */
    public function show($id)
    {
        $result         = parent::makeHttpRequest('get', 'charges/' . $id);
        return new Charge($result);
    }

    /**
     * @see     https://stripe.com/docs/api#create_charge
     * @param   CreateChargeRequest|array   $request
     * @return  Charge
     */
    public function store($request)
    {
        $data           = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        $result         = parent::makeHttpRequest('post', 'charges', $data);
        return new Charge($result);
    }

}