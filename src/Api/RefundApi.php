<?php

namespace jamesvweston\Stripe\Api;

use jamesvweston\Stripe\Models\Requests\Contracts\CreateRefundRequest;
use jamesvweston\Stripe\Models\Responses\Refund;

/**
 * @see https://stripe.com/docs/api#refunds
 * Class RefundApi
 * @package jamesvweston\Stripe\Api
 */
class RefundApi extends BaseApi
{

    /**
     * @see     https://stripe.com/docs/api#retrieve_refund
     * @param   string $id
     * @return  Refund
     */
    public function show($id)
    {
        $result         = parent::makeHttpRequest('get', 'refunds/' . $id);
        return new Refund($result);
    }

    /**
     * @see     https://stripe.com/docs/api#create_refund
     * @param   CreateRefundRequest|array   $request
     * @return  Refund
     */
    public function store($request)
    {
        $data           = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        $result         = parent::makeHttpRequest('post', 'refunds', $data);
        return new Refund($result);
    }

}