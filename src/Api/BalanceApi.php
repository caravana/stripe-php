<?php

namespace jamesvweston\Stripe\Api;


use jamesvweston\Stripe\Models\Responses\BalanceTransaction;

/**
 * @see https://stripe.com/docs/api#balance
 * Class BalanceApi
 * @package jamesvweston\Stripe\Api
 */
class BalanceApi extends BaseApi
{

    /**
     * @see     https://stripe.com/docs/api#retrieve_balance_transaction
     * @param   string  $id
     * @return  BalanceTransaction
     */
    public function showBalanceTransaction($id)
    {
        $result         = parent::makeHttpRequest('get', 'balance/history/' . $id);
        return new BalanceTransaction($result);
    }
}