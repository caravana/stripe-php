<?php

namespace jamesvweston\Stripe\Api;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use jamesvweston\Stripe\Exceptions\StripeException;
use jamesvweston\Stripe\Models\Requests\Contracts\Validatable;
use jamesvweston\Stripe\Models\Responses\Error;
use jamesvweston\Stripe\StripeApiConfiguration;
use jamesvweston\Utilities\ArrayUtil AS AU;

/**
 * Class BaseApi
 * @see     https://stripe.com/docs/api
 * @package jamesvweston\Stripe\Api
 */
class BaseApi
{

    /**
     * @var StripeApiConfiguration
     */
    protected $config;

    /**
     * @var     Client
     */
    private $guzzle;
    
    
    public function __construct(StripeApiConfiguration $stripeApiConfiguration)
    {
        $this->config               = $stripeApiConfiguration;
        $this->guzzle               = new Client();
    }

    /**
     * @param   string      $method
     * @param   string      $path
     * @param   array|null  $apiRequest
     * @param   array|null  $queryString
     * @return  mixed
     * @throws  StripeException, ClientException
     */
    protected function makeHttpRequest($method, $path, $apiRequest = null, $queryString = null)
    {
        if ($apiRequest instanceof Validatable)
            $apiRequest->validate();
        
        $urlEndPoint                = $this->config->getBaseURL() . '/' . $this->config->getVersion() . '/' . $path;

        $data   = [
            'headers'   => ['Authorization'     => 'Bearer ' . $this->config->getSecret()],
            'query'     => $apiRequest
        ];

        try
        {
            switch ($method)
            {
                case 'post':
                    $response       = $this->guzzle->post($urlEndPoint, $data);
                    break;
                case 'put':
                    $response       = $this->guzzle->put($urlEndPoint, $data);
                    break;
                case 'delete':
                    $response       = $this->guzzle->delete($urlEndPoint, $data);
                    break;
                case 'get':
                    $response       = $this->guzzle->get($urlEndPoint, $data);
                    break;
                default:
                    return null;
            }
        }
        catch (ClientException $ex)
        {
            $result                 = json_decode($ex->getResponse()->getBody()->getContents(), true);
            
            if (!is_null(AU::get($result['error'])))
            {
                $error              = new Error($result['error']);
                throw new StripeException($error, $ex->getCode());
            }
            else
            {
                throw $ex;
            }
        }
        
        $result = json_decode($response->getBody(), true);
        
        return $result;
    }

    /**
     * @return  StripeApiConfiguration
     */
    public function getStripeApiConfiguration()
    {
        return $this->config;
    }

    /**
     * @param   StripeApiConfiguration  $stripeApiConfiguration
     */
    public function setConfig(StripeApiConfiguration $stripeApiConfiguration)
    {
        $this->config           = $stripeApiConfiguration;
    }
    
}