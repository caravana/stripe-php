<?php

namespace jamesvweston\Stripe\Api;


use jamesvweston\Stripe\Models\Requests\Contracts\CreateTransferRequest;
use jamesvweston\Stripe\Models\Responses\Transfer;

/**
 * @see https://stripe.com/docs/api#transfers
 * Class TransferApi
 * @package jamesvweston\Stripe\Api
 */
class TransferApi extends BaseApi
{

    /**
     * @see     https://stripe.com/docs/api#create_transfer
     * @param   CreateTransferRequest|array     $request
     * @return  Transfer
     * @throws  \jamesvweston\Stripe\Exceptions\StripeException
     */
    public function store($request)
    {
        $data           = ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
        $result         = parent::makeHttpRequest('post', 'transfers', $data);
        return new Transfer($result);
    }

    /**
     * @see     https://stripe.com/docs/api#retrieve_transfer
     * @param   string $id
     * @return  Transfer
     * @throws  \jamesvweston\Stripe\Exceptions\StripeException
     */
    public function show($id)
    {
        $result         = parent::makeHttpRequest('get', 'transfers/' . $id);
        return new Transfer($result);
    }

}