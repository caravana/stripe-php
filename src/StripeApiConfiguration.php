<?php

namespace jamesvweston\Stripe;


use jamesvweston\Utilities\ArrayUtil AS AU;

class StripeApiConfiguration
{

    /**
     * @var     string
     */
    protected $baseURL      = 'https://api.stripe.com';

    /**
     * @var     string
     */
    protected $version;

    /**
     * @var     string
     */
    protected $secret;
    
    
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->secret               = AU::get($data['secret']);
            $this->version              = AU::get($data['version'], 'v1');
        }
    }

    /**
     * @return string
     */
    public function getBaseURL()
    {
        return $this->baseURL;
    }

    /**
     * @param string $baseURL
     */
    public function setBaseURL($baseURL)
    {
        $this->baseURL = $baseURL;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }
    
}