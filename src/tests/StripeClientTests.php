<?php

namespace jamesvweston\Stripe\tests;


use jamesvweston\Stripe\StripeClient;
use Dotenv\Dotenv;

class StripeClientTests extends \PHPUnit_Framework_TestCase
{

    public function testENVInstantiation()
    {
        $stripeClient               = new StripeClient('./');
        $this->assertInstanceOf('jamesvweston\Stripe\StripeClient', $stripeClient);
    }
    
    public function testArrayInstantiation()
    {
        $dotEnv                         = new Dotenv('./');
        $dotEnv->load();

        $data = [
            'stripeSecret'              => getenv('STRIPE_SECRET'),
            'version'                   => getenv('STRIPE_VERSION'),
        ];
        
        $stripeClient                   = new StripeClient($data);
        $this->assertInstanceOf('jamesvweston\Stripe\StripeClient', $stripeClient);
    }
}