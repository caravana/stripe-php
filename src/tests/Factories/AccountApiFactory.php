<?php

namespace jamesvweston\Stripe\tests\Factories;


use jamesvweston\Stripe\Models\Requests\CreateAccountRequest;
use jamesvweston\Stripe\Models\Requests\CreateBankAccountRequest;
use jamesvweston\Stripe\Models\Requests\CreateBankAccountSourceRequest;
use jamesvweston\Stripe\Models\Responses\Account;
use jamesvweston\Stripe\Models\Responses\Address;
use jamesvweston\Stripe\Models\Responses\BankAccount;
use jamesvweston\Stripe\Models\Responses\DOB;
use jamesvweston\Stripe\Models\Responses\LegalEntity;
use jamesvweston\Stripe\Models\Responses\TOSAcceptance;
use jamesvweston\Stripe\Utilities\Data\BankAccountUtil;

class AccountApiFactory extends BaseApiFactory
{

    /**
     * @return  Account
     */
    public function createAccount()
    {
        $createAccountRequest           = new CreateAccountRequest();
        $createAccountRequest->setManaged('true');
        return $this->stripeClient->accountApi->store($createAccountRequest);
    }

    /**
     * @param   Account   $account
     * @return  Account
     */
    public function createBankAccount($account)
    {
        $createBankAccountSourceRequest = new CreateBankAccountSourceRequest();
        $createBankAccountSourceRequest->setAccountHolderName('John Doe');
        $createBankAccountSourceRequest->setAccountHolderType('individual');
        $createBankAccountSourceRequest->setCurrency('USD');
        $createBankAccountSourceRequest->setCountry('US');

        $usAccount                      = BankAccountUtil::validUSAccount();
        $createBankAccountSourceRequest->setAccountNumber($usAccount['number']);
        $createBankAccountSourceRequest->setRoutingNumber($usAccount['routing']);

        $createBankAccountRequest       = new CreateBankAccountRequest();
        $createBankAccountRequest->setExternalAccount($createBankAccountSourceRequest);

        return $this->stripeClient->accountApi->storeBankAccount($account->getId(), $createBankAccountRequest);
    }

    /**
     * @param   Account   $account
     * @return  Account
     */
    public function updateAccount($account)
    {
        $legalEntity                    = new LegalEntity();
        $legalEntity->setFirstName('John');
        $legalEntity->setLastName('Doe');
        $legalEntity->setType('company');
        $legalEntity->setBusinessName('Whatever');


        $dob                            = new DOB();
        $dob->setDay('25');
        $dob->setMonth('10');
        $dob->setYear('2015');
        $legalEntity->setDob($dob);

        $address                        = new Address();
        $address->setCity('Savannah');
        $address->setLine1('24 East Liberty St');
        $address->setPostalCode('31401');
        $address->setState('GA');
        $legalEntity->setAddress($address);

        date_default_timezone_set('UTC');
        $ts                             = new \DateTime();
        $tosAcceptance                  = new TOSAcceptance();
        $tosAcceptance->setDate($ts->getTimestamp());
        $tosAcceptance->setIp('216.119.48.74');


        $test = [
            'legal_entity'      => $legalEntity->jsonSerialize(),
            'tos_acceptance'    => $tosAcceptance->jsonSerialize()
        ];

        $test['legal_entity']['business_tax_id'] = '812642790';
        $test['legal_entity']['ssn_last_4'] = '1111';
        
        $account    = $this->stripeClient->accountApi->update($account->getId(), $test);

        return $account;
    }

    /**
     * @param   string  $accountId
     * @return  Account
     */
    public function showAccount($accountId)
    {
        return $this->stripeClient->accountApi->show($accountId);
    }

    /**
     * @param   string      $accountId
     * @param   string      $bankAccountId
     * @return  BankAccount
     */
    public function showAccountBankAccount($accountId, $bankAccountId)
    {
        return $this->stripeClient->accountApi->showBankAccount($accountId, $bankAccountId);
    }

    /**
     * @return bool
     */
    public function deleteAccount()
    {
        $account                        = $this->createAccount();
        return $this->stripeClient->accountApi->delete($account->getId());
    }
    
}