<?php

namespace jamesvweston\Stripe\tests\Factories;


use jamesvweston\Stripe\Models\Requests\CreateTransferRequest;
use jamesvweston\Stripe\Models\Responses\Account;
use jamesvweston\Stripe\Models\Responses\Charge;
use jamesvweston\Stripe\Models\Responses\Transfer;

class TransferApiFactory extends BaseApiFactory
{

    /**
     * @param   Charge  $charge
     * @param   Account $account
     * @return  Transfer
     */
    public function transferFromChargeToManagedAccount($charge, $account)
    {
        $createTransferRequest      = new CreateTransferRequest();
        $createTransferRequest->setAmount('300');
        $createTransferRequest->setCurrency('USD');
        $createTransferRequest->setDestination($account->getId());
        $createTransferRequest->setSourceTransaction($charge->getId());
        
        return $this->stripeClient->transferApi->store($createTransferRequest);
    }
}