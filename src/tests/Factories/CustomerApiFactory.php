<?php

namespace jamesvweston\Stripe\tests\Factories;


use jamesvweston\Stripe\Models\Requests\CreateBankAccountRequest;
use jamesvweston\Stripe\Models\Requests\CreateBankAccountSourceRequest;
use jamesvweston\Stripe\Models\Requests\CreateCardRequest;
use jamesvweston\Stripe\Models\Requests\CreateCardSourceRequest;
use jamesvweston\Stripe\Models\Requests\CreateCustomerRequest;
use jamesvweston\Stripe\Models\Requests\ListCustomersRequest;
use jamesvweston\Stripe\Models\Responses\BankAccount;
use jamesvweston\Stripe\Models\Responses\Card;
use jamesvweston\Stripe\Models\Responses\Collections\CustomerCollection;
use jamesvweston\Stripe\Models\Responses\Customer;
use jamesvweston\Stripe\Utilities\Data\BankAccountUtil;

class CustomerApiFactory extends BaseApiFactory
{

    /**
     * @return  Customer
     */
    public function createCustomer()
    {
        $createCustomerRequest          = new CreateCustomerRequest();
        $createCustomerRequest->setEmail('john.asdf@asdfasdf.com');
        return $this->stripeClient->customerApi->store($createCustomerRequest);
    }

    /**
     * @param   string  $customerId
     * @return  Customer
     */
    public function showCustomer($customerId)
    {
        return $this->stripeClient->customerApi->show($customerId);
    }

    /**
     * @param   Customer    $customer
     * @param   string      $email
     * @return  Customer
     */
    public function updateCustomerEmail($customer, $email)
    {
        $newEmail                       = $email;
        $customer->setEmail($newEmail);
        return $this->stripeClient->customerApi->update($customer);
    }

    /**
     * @return  CustomerCollection
     */
    public function listCustomers()
    {
        $listCustomersRequest           = new ListCustomersRequest();
        return $this->stripeClient->customerApi->index($listCustomersRequest);
    }

    /**
     * @return bool
     */
    public function deleteCustomer()
    {
        $customer                       = $this->createCustomer();
        return $this->stripeClient->customerApi->delete($customer->getId());
    }

    /**
     * @param   Customer    $customer
     * @return  BankAccount
     */
    public function createBankAccountForCustomer($customer)
    {
        $createBankAccountSourceRequest = new CreateBankAccountSourceRequest();
        $createBankAccountSourceRequest->setAccountHolderName('John Doe');
        $createBankAccountSourceRequest->setAccountHolderType('individual');
        $createBankAccountSourceRequest->setCurrency('USD');
        $createBankAccountSourceRequest->setCountry('US');

        $usAccount                      = BankAccountUtil::validUSAccount();
        $createBankAccountSourceRequest->setAccountNumber($usAccount['number']);
        $createBankAccountSourceRequest->setRoutingNumber($usAccount['routing']);

        $createBankAccountRequest       = new CreateBankAccountRequest();
        $createBankAccountRequest->setSource($createBankAccountSourceRequest);

        return $this->stripeClient->customerApi->storeBankAccount($customer->getId(), $createBankAccountRequest);
    }

    /**
     * @param   Customer    $customer
     * @return  Card
     */
    public function createCardForCustomer($customer)
    {
        $createCardSourceRequest        = new CreateCardSourceRequest();
        $createCardSourceRequest->setName('John Doe');
        $createCardSourceRequest->setNumber('4242424242424242');
        $createCardSourceRequest->setExpMonth('08');
        $createCardSourceRequest->setExpYear('2020');
        $createCardSourceRequest->setCvc('323');
        $createCardSourceRequest->setAddressZip('31401');


        $createCardRequest              = new CreateCardRequest();
        $createCardRequest->setSource($createCardSourceRequest);
        
        return $this->stripeClient->customerApi->storeCard($customer->getId(), $createCardRequest);
    }

}