<?php

namespace jamesvweston\Stripe\tests\Factories;


use jamesvweston\Stripe\StripeClient;

class BaseApiFactory
{

    /**
     * @var StripeClient
     */
    protected $stripeClient;

    public function __construct($stripeClient)
    {
        $this->stripeClient             = $stripeClient;
    }
    
}