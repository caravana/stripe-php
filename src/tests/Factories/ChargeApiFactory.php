<?php

namespace jamesvweston\Stripe\tests\Factories;


use jamesvweston\Stripe\Models\Requests\CreateChargeRequest;
use jamesvweston\Stripe\Models\Responses\Card;
use jamesvweston\Stripe\Models\Responses\Charge;
use jamesvweston\Stripe\Models\Responses\Customer;

class ChargeApiFactory extends BaseApiFactory
{

    /**
     * @param   Customer    $customer
     * @param   Card        $card
     * @return  Charge
     */
    public function chargeCustomerCard($customer, $card)
    {
        $createChargeRequest        = new CreateChargeRequest();
        $createChargeRequest->setAmount('10000');
        $createChargeRequest->setCurrency('USD');
        $createChargeRequest->setCapture('true');
        $createChargeRequest->setCustomer($customer->getId());
        $createChargeRequest->setSource($card->getId());
        $createChargeRequest->setStatementDescriptor('Caravana Rentals');
        
        return $this->stripeClient->chargeApi->store($createChargeRequest);;
    }
}