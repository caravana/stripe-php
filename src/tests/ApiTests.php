<?php

namespace jamesvweston\Stripe\tests;


use jamesvweston\Stripe\Models\Responses\Account;
use jamesvweston\Stripe\Models\Responses\BalanceTransaction;
use jamesvweston\Stripe\Models\Responses\BankAccount;
use jamesvweston\Stripe\Models\Responses\Card;
use jamesvweston\Stripe\Models\Responses\Charge;
use jamesvweston\Stripe\Models\Responses\Customer;
use jamesvweston\Stripe\StripeClient;
use jamesvweston\Stripe\tests\Factories\AccountApiFactory;
use jamesvweston\Stripe\tests\Factories\ChargeApiFactory;
use jamesvweston\Stripe\tests\Factories\CustomerApiFactory;
use jamesvweston\Stripe\tests\Factories\TransferApiFactory;

class ApiTests extends \PHPUnit_Framework_TestCase
{

    /**
     * @var StripeClient
     */
    private $stripeClient;

    /**
     * @var Account
     */
    private $account;

    /**
     * @var BankAccount
     */
    private $accountBankAccount;
    
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Charge
     */
    private $customerCharge;

    /**
     * @var Card
     */
    private $customerCard;

    /**
     * @var BankAccount
     */
    private $customerBankAccount;

    /**
     * @var BalanceTransaction
     */
    private $balanceTransaction;

    public function testApis()
    {
        $this->stripeClient             = new StripeClient('./');
        
        $this->customerApiTests();
        $this->accountApiTests();
        $this->chargeApiTests();
        $this->balanceApiTests();
        $this->transferApiTests();
    }
    
    private function customerApiTests()
    {
        $customerApiFactory             = new CustomerApiFactory($this->stripeClient);
        
        //  Create
        $this->customer                 = $customerApiFactory->createCustomer();
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Customer', $this->customer);
        
        //  Show
        $this->customer                 = $customerApiFactory->showCustomer($this->customer->getId());
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Customer', $this->customer);
        
        //  Update
        $newEmail                       = 'whoever@whatever.com';
        $this->customer                 = $customerApiFactory->updateCustomerEmail($this->customer, $newEmail);
        $this->assertEquals($newEmail, $this->customer->getEmail());
        
        //  List
        $customerCollection             = $customerApiFactory->listCustomers();
        $this->assertInstanceOf('jamesvweston\Stripe\Models\Responses\Collections\CustomerCollection', $customerCollection);
        
        //  Delete
        $deleteResult                   = $customerApiFactory->deleteCustomer();
        $this->assertEquals(1, $deleteResult);

        //  Create bank account for customer
        $this->customerBankAccount      = $customerApiFactory->createBankAccountForCustomer($this->customer);
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\BankAccount', $this->customerBankAccount);
        
        //  Create card for customer
        $this->customerCard             = $customerApiFactory->createCardForCustomer($this->customer);
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Card', $this->customerCard);
    }
    
    private function accountApiTests()
    {
        $accountApiFactory              = new AccountApiFactory($this->stripeClient);
        
        //  Store
        $this->account                  = $accountApiFactory->createAccount();
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Account', $this->account);
        
        //  Create bank account for account
        $this->accountBankAccount       = $accountApiFactory->createBankAccount($this->account);
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\BankAccount', $this->accountBankAccount);
        
        //  Update
        $this->account                  = $accountApiFactory->updateAccount($this->account);
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Account', $this->account);

        //  Show
        $this->account                  = $accountApiFactory->showAccount($this->account->getId());
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Account', $this->account);

        //  Show bank account for account
        $this->accountBankAccount       = $accountApiFactory->showAccountBankAccount($this->account->getId(), $this->accountBankAccount->getId());
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\BankAccount', $this->accountBankAccount);

        //  Delete
        $this->assertEquals(1, $accountApiFactory->deleteAccount());
    }
    
    private function chargeApiTests()
    {
        $chargeApiFactory               = new ChargeApiFactory($this->stripeClient);
        
        //  Charge customer card
        $this->customerCharge           = $chargeApiFactory->chargeCustomerCard($this->customer, $this->customerCard);
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Charge', $this->customerCharge);

        //  Get the charge
        $charge                         = $this->stripeClient->chargeApi->show($this->customerCharge->getId());
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Charge', $charge);
    }

    private function balanceApiTests()
    {
        $this->balanceTransaction       = $this->stripeClient->balanceApi->showBalanceTransaction($this->customerCharge->getBalanceTransaction());
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\BalanceTransaction', $this->balanceTransaction);
    }
    
    private function transferApiTests()
    {
        $transferApiFactory             = new TransferApiFactory($this->stripeClient);
        
        //  Store
        $transfer                       = $transferApiFactory->transferFromChargeToManagedAccount($this->customerCharge, $this->account);
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Transfer', $transfer);
        
        //  Show
        $showTransferResponse           = $this->stripeClient->transferApi->show($transfer->getId());
        $this->assertInstanceOf('\jamesvweston\Stripe\Models\Responses\Transfer', $showTransferResponse);
    }
    
    
}